# Data Engineering Community Impact

This circle has the purpose to

> _"Create an impact on data engineering community at worldwide level"._

This derives from the global purpose of _"... elevating the Data Engineering game"_.

As described in the [principles and pillars page](DataEngineering.md), Elite Data Engineering is core for us, thus we would like to also bring our vision, expertise, and contribution to the worldwide community.

The main activities carried out by the circle are:

- Open Source contribution, to internal and worldwide projects, as described in detail in the [dedicated section](OpenSource.md);
- Creation of [knowledge base contents](https://www.agilelab.it/blog/tag/knowledge-base), such as articles and more following the process and the guidelines defined by Marketing;
- Participation to the activities of global data engineering/management related communities (e.g. [Data Mesh Learning Community](https://datameshlearning.com/people-to-follow/));

Although there are some specific roles for these activities, any contribution is always more than welcome!

Please refer to the [Team Topologies](TeamTopologies.md) page to understand the circle placement in the overall value streams strategy.

If you want coaching sessions on technical writing or public speaking, technical review of articles, or conference talks, please refer to the {% role %}Data Engineering community impact/Chiara Ferragni{% endrole %} role for guidelines.
