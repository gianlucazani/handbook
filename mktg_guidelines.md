#Introduction

As we all know, marketing is the set of activities that aim to shape and strengthen a brand's image and effectively communicate it to its audience. Agile Lab is making great efforts to expand its reach and present itself on the market as a recognizable brand with its own story: for the company to keep going along this path, we need everyone to collaborate as a team.

The following marketing guidelines illustrate where we are, who we hope to be in the near future and what practices are in our best interest to reach our goals. Each and every one of us represents the company, so we can all enrich and improve its image. Our guidelines aren't supposed to limit us, they're supposed to incentivize proper processes.

#Process

Agile Lab is currently present on several social networks and online platforms, such as LinkedIn, Instagram, Medium, YouTube, Meetup and Twitter. Lead generation isn't the only goal of our marketing communications plan: we also commit to building a community of knowledgeable engineers, sharing our latest success stories and improvements with new technologies, and recruiting brilliant talent on the job market.

We do this through:

* publishing both technical and business articles, that add value to our brand
* organizing meetups, events and interviews
* organizing business-oriented events, such as webinars
* establishing partnerships
* taking part in contests

We communicate with both engineers and managers, so it is fundamental to know your audience and communicate accordingly.

In addition, it's important to remember to express preferences without diminishing someone else's value or opinion.

#Roles

As a company based on self-management, we have established communication roles, and each role has specific responsibilities:

* **Social Publisher**: The only role who's allowed to post branded content on our social channels. For every channel, they are in charge of deciding what it's best to post and when.
* **Content Owner**: They are in charge of creating content (writing posts and articles) and ensure that everything is correct before they hand in their work to the social publisher. They decide what content should be published under the company's name.
* **Event Specialist**: They are in charge of setting up events for the company. They plan any established company events and come up with ideas for new ones. The details of the event planning process are their responsibility, and they also communicate with social publishers to advertise the event together (and potentially edit and publish any videos)

#Contributing

Everyone is welcome (and encouraged) to contribute to our social media presence through their personal accounts, as long as:
* when we tag Agile Lab, we must ensure that the content pertains to our company, our scope of technologies and our strategy
* re-sharing Agile Lab's content: any employee who does have a personal account, is highly encouraged to reshare our content and help us reach an even bigger audience. Any engagement with our social media content (likes, shares, comments etc.) can contribute immensely to our marketing efforts

Also, it is fundamental to keep checking in our website: we must know what our company is "selling" and keep on track with any product/service upgrades

Before engaging with any of Agile Lab's content, please note that:

We write in respect of everyone's ethnicity, religion, culture and thoughts regardless of whether we can relate or not. Disagreeing is legit and proves a person's critical thinking, but we must remember to calibrate the delivery of every thought we express.

If there's any controversy involving Agile Lab, we must never address it directly, but rather report the mishap to our marketing department.

Since we all know by now how fundamental privacy is, we must never share any sensitive information, whether it regards our clients or Agile Lab. We must remember to always receive approval before we post anything about anyone, and we must never mention our clients unless they specifically ask.

Please be careful when mentioning competitors: we must never diminish their work. On social media, we should always talk positively about our competition.

Whenever we post on social media, the content we publish represents the brand. We must remember that posting branded content (especially if our logo is on it) is like putting a signature on our online material. Whatever we post and publish is always going to be associated with our company name.

For this very reason, we must always respect the decisions made by the company. We're all entitled to express our opinion, but if it isn't in line with our company's thoughts, whoever is posting is

supposed to specify that they're expressing their own point of view (which, in that case, wouldn't reflect that of the company).

Agile Lab celebrates its employees' unique talents and skills: we're all recognized as experts in our respective fields, so dismissing or denying anyone's opinion would damage the image of the company, as well as the people who work for Agile Lab. 