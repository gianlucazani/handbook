# Book Club

The Book Club is a gym where we read to expand our knowledge and inspire knowledge sharing.

The goal of the Book Club is to promote:
- learning something new (continuous learning)
- learning something useful (real-world applications)
- collaborative learning (inspire and be inspired).

The Book Club is an initiative of the Training Circle. It is run by the {% role %}Training/Book Club curator{% endrole %}.

## Summary
- [How do we train with books?](#how-do-we-train-with-books)
- [How can I start training with the Book Club?](#how-can-i-start-training-with-the-book-club)
- [How can I buy the current book?](#how-can-i-buy-the-current-book)
- [Can I use company time for Book Club training?](#can-i-use-company-time-for-book-club-training)
- [How do Book Club courses work?](#how-do-book-club-courses-work)
- [How do Book Club Curators kick off a new training program?](#how-do-book-club-curators-kick-off-a-new-training-program)

### How do we train with books?
In our reading gym, we train with books iteratively. A reading iteration is usually [timeboxed](https://www.agilealliance.org/glossary/timebox/) at two weeks.

Every reading iteration,
- We read something new (eg book chapter, blog post) in async.
- We collect all contributions you'd like to share about the topic we read about. Contributions include:
  - questions about key ideas from the book,
  - questions about related topics,
  - contents (e.g. videos, blog posts, quotes, or chapters from different books) related to key ideas from the book (e.g. explanations, applications, deep dives),
  - lessons learned from applying key ideas from the book to a real-world data project.
- We meet (30min) to discuss what we read. [How does the discussion work?](#how-does-the-discussion-work)
- We distill key concepts and lessons learned from the reading/discussion as an [LMS Course](Courses.md). [How do Book Club courses work](#how-do-book-club-courses-work)

### How can I start training with the Book Club?
You can start training with us at any time.
- Check on the Dev>book-club channel on Teams what is the book we are currently reading.
- If you wish so, you can buy the book. [How can I buy the current book?](#how-do-i-buy-the-current-book)
- To catch up, you can:
    - read the quick notes and challenge yourself with quizzes on LMS
    - read the summary session notes on the Dev>book-club channel
- If you need any help, you can Write a message to the {% role %}Training/Book Club curator{% endrole %}.

#### How can I buy the current book?
When joining the Book Club, you can purchase the current book and get reimbursement.

- Please check on Holaspirit who currently energizes the role of {% role %}Training/Book Club curator{% endrole %}.
- Write a message to the {% role %}Training/Book Club curator{% endrole %} and ask what is the current book.
- You can buy the digital or the paperback book and get full reimbursement.
	On Elapseit, record *one* expense: 
    
    1) Use the project: `<current year> Agile Training - Book Club Curator`

For more info about the general purchasing process, see scenario 1 in [Spending Company Money](SpendingCompanyMoney.md) .

#### Can I use company time for Book Club training?
Yes, you can! You can safely use company time (Training time budget) to train with books.
	
If needed, you can choose to use company time (Training time budget) for reading, contributing and attending sessions.

This means you need to track Book Club activities on ElapseIt as explained in the table below:

| Activity | Project on ElapseIt |
|----------|---------------------|
|Reading outside office hours|Nothing to track|
|Reading during office hours|`<current year> Agile Training - Personal Budget`|
|Session (during office hours)|`<current year> Agile Training - Personal Budget`|

For more info about Training time budget, see Training Budget in [Benefits](Benefits.md).

### How do Book Club courses work?
We create content for [LMS Courses](Courses.md) with distilled learnings from the reading/discussion in order to:

1. have an extra opportunity to process and review what we learned collaboratively at every reading iteration - think [protege effect](https://effectiviology.com/protege-effect-learn-by-teaching/) with gamification,
2. crunch knowledge from books for the circle:Engine Circle,
3. enable anytime onboarding of new readers,
4. enable anytime catch-up for paused readers - life happens and that's ok!

### How do Book Club Curators kick off a new training program?
When a book is completed, we choose a new book.
1. Book Club Curators collect strategic topics for continuous learning according to the tech strategy.
2. Book Club Curators select at least a book for each strategic topic.
3. Book Club Curators share the selected books in a poll to all participants of the Engine Circle.
4. All interested readers vote the books(s) they are interested to read in the Book Club, based on current personal interest/goals.
5. Book Club Curators communicate the chosen book - meaning the top voted book - to all participants of the Engine Circle.
6. Book Club Curators communicate next steps, including reading calendar, book purchasing details and reading scope for the first iteration.
7. The reading calendar describes reading iterations. Each reading iteration includes:
  - *Start date*. This is the date Book Club Curators recommend starting reading new material (typically a new book chapter).
  - *Training session date*. This is a sync session where you can [train with us on the book ideas](#how-do-we-train-with-books).

