# Contributing to Open Source

Agile Lab is a proud user of open-source technologies, we have received useful tools from the community from the time being, and we want to give back by contributing to open-source development. Furthermore, we would like to embrace our purpose to elevate the Data Engineering game by contributing with our expertise, ideas, vision to the open-source worldwide community, with the final goal to make a significant impact.

Being able to contribute to open-source projects will improve your skills all around and the company will benefit from this. It is a win-win approach and also a good way to raise the bar of our technical and organizational skills.

## Contributing to mainstream projects

We'd like people to be engaged, while self-managing themselves. For this reason, many points of the process that was valid in the past have been removed. The current process is:

1. Find a project of your interest, as long as it's in the Data Engineering space. (NOTE: if you want to contribute as OSS to other projects out of this scope you're more than encouraged but that would fall under another budget).
2. Find an issue of your interest in that project, take ownership, and use `<current year> Agile Community Impact - activities` elapseit project to track the time spent.
3. Create an entry in the [tracking spreadsheet](https://agilelab.sharepoint.com/:x:/s/communityimpact/EddqqXhJKYRLogGwtXUpWeMBnwVCm13YfMvZmykPcdvkIA?e=dC5I7N) specific page, engage with the {% role %}Data Engineering community impact/Indiana Jones{% endrole %} role for a brief design discussion (NOTE: for mainstream OSS projects, contributions don't need formal approval. If the project to contribute to is a very corner one, the Indiana Jones role could provide some strategical guidelines).
4. From a capacity perspective, time to work on OSS needs to be carved out of your day-by-day activities, obviously respecting 2 simple self-management rules:
  - Discuss if needed about the capacity with your coach: delivery and customer satisfaction should **never** be at risk;
  - Time spent on these activities, from a timesheet perspective, should be managed just like the activities in other circles (i.e., not be paid by the customer).
5. While working on your task, feel free to engage with your BU's mentors for peer reviewing and mentorship, as well as the usual internal Dev channels.
6. Once the PR has been merged with the mainstream project, or even if it has been rejected (there's always something to learn!), update the [tracking spreadsheet](https://agilelab.sharepoint.com/:x:/s/communityimpact/EddqqXhJKYRLogGwtXUpWeMBnwVCm13YfMvZmykPcdvkIA?e=dC5I7N) and post a message with link of the PR in the `Dev/Open Source` Teams channel. If merged, there's an high chance that Marketing will be eager to give it visibility on Agile Lab's social channels.

Everyone is free to integrate the time provided by the company using their own free time.

## Contributing to internal projects

Agile Lab has also released several internal projects as open-source on its [GitHub](https://github.com/agile-lab-dev).

If you want to contribute to these projects you're more than welcome. However, as of the current strategy, the time budget for this contribution should derive from explicit need for changes expressed by our customers adopting our software. In such case, the activity shall be discussed with the project's specific {% role %}Data Engineering community impact/Open Source Contributor{% endrole %}.

## Proposing new internal projects

In the first page of [tracking spreadsheet](https://agilelab.sharepoint.com/:x:/s/communityimpact/EddqqXhJKYRLogGwtXUpWeMBnwVCm13YfMvZmykPcdvkIA?e=dC5I7N) there's a list of projects eager to see the light. If any of them excite your interest, discuss with one of the {% role %}Data Engineering community impact/Indiana Jones{% endrole %} role fillers about the opportunity to contribute. 

Also, if you have new original ideas for open-source projects, add them to the [tracking spreadsheet](https://agilelab.sharepoint.com/:x:/s/communityimpact/EddqqXhJKYRLogGwtXUpWeMBnwVCm13YfMvZmykPcdvkIA?e=dC5I7N) and discuss them with a {% role %}Data Engineering community impact/Indiana Jones{% endrole %} role filler who will decide an execution strategy.
