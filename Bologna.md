# Bologna

## How to reach us

- Address: [Crea System - Galleria Ugo Bassi, 1, 40121 BO](https://goo.gl/maps/XmMvSG4qZLQ2tuJv8)
  - [Website](https://www.creasystem.it/)
- 20 minutes' walk from the station
- Parking: [Garage](https://goo.gl/maps/59S1MuiSW86Dz6NJ6) with Crea System agreement; to get the reduced rate, 
  simply go to reception with your parking ticket and stamp it with the Crea System logo. [Check the ZTL permissions](https://www.comune.bologna.it/servizi-informazioni/zona-traffico-limitato-ztl).
- By bus (from [station](https://goo.gl/maps/ogvk3pBSiUihh4bU7)):
  * Tper Line 25 from *Stazione Centrale* to *Ugo Bassi* stop
  * Tper Line 15 from *Piazza XX Settembre* to *Ugo Bassi* stop
  * Tper Line 30 from *Stazione Centrale* to *Ugo Bassi* stop
- At the main door:
  - Button "Crea System" for the reception
  - Dial 42 to ring the office bell directly

## Office
- Bologna employees with registered fingerprint can access the office everytime, at any date and hour (also weekend).
- Reception times: Monday to Friday from 9 am to 7 pm  
- Rooms: 
    * 1 private office room with 8 desks
    * 2 conference rooms (to be booked in advance with extra costs - please refer to the Bologna [Facility Manager](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5e99064a094ee80565620bab) for instructions):
      * Meeting room 04, at *Via Nazario Sauro 4* (from 10 to 25 seats) 
      * Meeting room 15, at *Galleria Ugo Bassi 1* (up to 10 seats)
