# Agile Lab's Integrated Management System (IMS)

Agile Lab's Integrated Management System (IMS) is a collection of policies and procedures that guide the company's activities to ensure compliance with regulations, effective risk management, and the achievement of business objectives. Additional operational documentation related to the IMS is available on the Compliance website on the company's Sharepoint.

The IMS of Agile Lab encompasses various thematic areas, including privacy, information security, risk management, quality, and other areas relevant to the company. Each area has its specific policies and procedures that define responsibilities, activities, and guidelines to follow.

To ensure the effectiveness of the IMS, it is essential for all Agile Lab staff to be aware of the relevant policies and procedures and adhere to them diligently in their work activities. The dissemination of policies and procedures occurs through the Compliance website on the company's Sharepoint, where additional operational documentation can be accessed.

The company is committed to keeping the IMS up to date and aligned with current regulations, continually monitoring regulatory changes and making necessary adjustments. Additionally, regular internal reviews and audits are conducted to assess the effectiveness of the IMS and identify opportunities for improvement.

Agile Lab's IMS reflects the company's dedication to ensuring regulatory compliance, effective risk management, and the adoption of best practices to deliver high-quality products and services to its customers.

Additional documents, checklists and others are available on che Compliance Sharepoint.