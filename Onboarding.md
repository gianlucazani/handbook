The onboarding process for the Engineering Department is the starting point for all the new people joining Agile Lab (regardless of status, interns should be onboarded in the same way).

# Checklist for Administration

- [ ] Inform Internal IT about new incoming members (starting date) via mail.
- [ ] Inform Internal IT about devices that will be delivered to the new member (notebook, cell phone, others...)

# Checklist for Internal IT

- [ ] Create an account on Office365 "name.surname@agilelab.it" and send credentials to the private email.
- [ ] Double check ( also on AD ) if MFA has been enabled
- [ ] Include it on group "dev".
- [ ] Include it on "BigData" Sharepoint.
- [ ] Include it on the specific branch (city) group.
- [ ] Check that new member devices are compliant

# Checklist for the COO
- [ ] Create an account on "Elapseit" and send credentials to "agilelab.it" email.
- [ ] Assign an onboarding buddy on the new employee's team.
- [ ] Send an email to the new employee, the buddy, and the first BU lead/Circle lead linking this page to let them be aware about next steps.
- [ ] Share the tech interview outcome with the assigned buddy so to allow her/him to perform a complete tech assessment of the newcomer.
- [ ] Include it on first project Sharepoint.
- [ ] Include it on recurring activities and meetings for the first project.


# Checklist for the BU Lead/Circe lead
- [ ] Add in the email thread the first team leader.
- [ ] Organize smooth onboarding with clear tasks. Define which technologies should be studied in the first two weeks.

# Checklist for Buddy
- [ ] Read the buddy section in the [leadership guidelines](https://publicagilefactory.gitlab.io/handbook/Leadership.html)
- [ ] Introduce yourself to the new team member with a video call.
- [ ] Remind about the handbook, suggest most useful pages, clarify it in case of need.
- [ ] Describe self-management practices, provide a high-level overview of the circles and invite the new colleague to attend as a guest a tactical meeting.
  Make sure they know that they are highly encouraged to join a circle and that they know how to proceed if they want to join one.
- [ ] Describe the Data Engineering practice documents, their purpose and how they contribute to elevate the data engineering game. Suggest that the newcomer reads [Pratices page](https://agilefactory.gitlab.io/developers/practices/) and review with them some key lessons
- [ ] Describe the OKRs goal-setting framework with the support of the [OKR page](https://publicagilefactory.gitlab.io/handbook/OKR.html). Suggest that the newcomer watch the OKR workshop on LMS and the resources provided in the OKR Teams channel (under the Resource tab)
- [ ] Talk about Agile Lab values and how to collaborate with peers.
- [ ] Explain compliance procedures and GDPR policies.
- [ ] Talk about the engineering ladder.
- [ ] Introduce her/him to Teams (note: new team member will be automatically inserted on major channels).
- [ ] Check in regularly about her/his first tasks status and clarify doubts.
- [ ] Support her/him on training tasks for the first week. The training program must be developed considering the subjects addressed by the Manager and the skill-matrix-based assessment in order to also fill up the knowledge gaps on "first class citizens" skills like version control tools and principles, issue tracking, SW development best practices and patterns, unit testing, etc ...
- [ ] Introduce the new team-member to 5 people and keep track of these people filling in the `sharepoint://Training/Documents/Buddy/onboarding_introduction_calls.xlsx` file. The new team-member's name should be added to the list in order to make him/her available for future calls. Please, don't schedule welcome calls with other Buddies: they would love to help, but they are already engaged in onboarding other people, so let's try to distribute the pleasure across the entire organization
- [ ] Talk about the [software development channel](https://web.microsoftstream.com/channel/e9dec84c-87c6-430a-b109-1964847d6bfe) on streams, since there some useful videos for training and deep dives
- [ ] Talk about the role of the {% role %}The elevator/Data Practice Mentor{% endrole %} before to say goodbye.
- [ ] Talk about the [awesome resources repo](https://gitlab.com/AgileFactory/developers/awesome-resources)
- [ ] Talk about [stackoverflow for teams](https://stackoverflow.com/c/agilelab/questions), (our internal StackOverflow), the benefit of it, like sharing issues (not only technical), and solutions so that all the company can benefit from it. Invite (on StackOverflow page, users -> invite users) every new ENG 1 team member or new members that seem very engaged by this initiative.
- [ ] Talk about Elapseit and explain how track the time and the expense in Agile Lab
- [ ] Share an onboarding outcome with the Manager and the first Project Lead the new member will be assigned to.
- [ ] Share the list of workplace safety managers (RLS, first aid, fire prevention) to the new members. The list is available in `sharepoint://BigData/Documents/People Operations/Responsabili sicurezza`

# Checklist for the first team leader
- [ ] Introduce yourself to the new team member with a video call.
- [ ] Get in touch with the project/BU lead to understand which tech stack the new hire should focus on.
- [ ] Support her/him on training tasks. The training program must be developed considering the subjects addressed by the Manager and the skill-matrix-based assessment in order to also fill up the knowledge gaps on "first class citizens" skills like version control tools and principles, issue tracking, SW development best practices and patterns, unit testing, etc ...
- [ ] Ask the new team member to fill-up the Skill Matrix.
- [ ] Leveraging the skill matrix and the tech interview outcome received from the Manager, perform a tech assessment of the new team member.
- [ ] Share an onboarding outcome with the Manager focused on the completed training program so to bring light over strong and weak points

# Checklist for new team-member

- [ ] Login into Office365 and check provided credentials are working.
- [ ] Login on Elapseit and check provided credentials are working.
- [ ] Login on Holaspirit and check provided credentials are working.
- [ ] Login on Teams.
- [ ] Check you can access to "BigData" Sharepoint.
- [ ] Follow [this guide](DeviceEnrollment.md) to enroll your devices.
- [ ] Contact Internal IT to verify that installation is completed or if you need help.
- [ ] Introduce yourself on the "intros" Teams channel so to let your (especially remote) colleagues know a little bit more about yourself, what do you like, area of expertise, location, etc. Although this is not mandatory, it's highly recommended for the sake of the value of being a company focused on the individuals as persons, not as just anonymous workers. Make sure to write your message in English so that all your colleagues can learn about yourself.
- [ ] Configure your Office365 profile picture. Use a photo of yourself to make you recognizable by other agile lab employees.
- [ ] As soon as you receive the Welcome Pack, register the received HW by filling out the following form: https://forms.office.com/e/Bt59qfYdgR
- [ ] Schedule a call (30 minute) with the {% role %}Training/Certification Coach{% endrole %} in order to know the certification plan and the related opportunities
- [ ] Schedule 5 introductory calls (30 minute each) with the 5 people provided by the buddy.
- [ ] Exploit the alignment sessions with your buddy to ask any clarifications (technical, about the company, the internal processes, etc) you might need.
- [ ] Add your signature in the Outlook email.
- [ ] Send a photo of yourself and your role to {% role %}Digital/Social media specialist{% endrole %} for the welcome post on LinkedIn and Instagram.
- [ ] Fill in the Excel `sharepoint://BigData/Documents/Sito Internet/Sito_paginaTeam.xlxs` to give authorisations and information to public your profile into AgileLab site. It's not mandatory to have the profile in Agile website: in this case you should fill the authorisations fields with `no`. In the future, you can change some information setting the field `status` with `ToBeModified`
- [ ] Fill in the data in the directory `sharepoint://BigData/Documents/Sito Internet/Sito_paginaTeam` with you name, surname, role, phrase/quotation that represents you, link of you LinkedIn profile.
- [ ] Add your photo (named `<name>_<surname>`) in the directory `sharepoint://BigData/Documents/Sito Internet/FotoPaginaTeam/`. This step is optional but recommended.
- [ ] At the end of the first two weeks, fill out the Onboarding Quiz: for the Engine people there is this form `sharepoint://BigData/Documents/Onboarding/Engine onboarding Quiz.url`, for others (marketing, sales, HR, etc.) there is `sharepoint://BigData/Documents/Onboarding/GCC onboarding Quiz.url`

  Outlook *app* users: access instructions and template in `sharepoint://BigData/Documents/Doc Templates/Template firme mail.docx`.

  Outlook *web* users:
  - open Firefox
  - access the template in `sharepoint://BigData/Documents/Doc Templates/FIRMA MAIL/firma email agile lab.htm`
  - select all (CTRL+A) and copy (CTRL+C)
  - open Outlook
  - in Settings, search Email Signature
  - in the signature text field, remove all contents
  - paste (CTRL+V) the templated signature
  - fill in your details (full name, title, email, office address).

# First two weeks

In Agile Lab, the first two weeks are dedicated to train on different technologies based on Manager indications and to explaining the company culture.
The owner of the company culture and operative information is the Buddy. This part should be done mainly during the first week.
The owner of the tech training, following the manager indications about different technologies, is the first team leader. The tech training should start in the first week, but it should become more consistent in the second week.
For the new employee, the first week will be tracked in Elapseit with the project 'YYYY Agile Training - Onboarding'. The project for the second week will be suggested by the first team leader.
The time spent by the buddy (usal 5-6 hours for each onborading) will be tracked also with the project 'YYYY Agile Training - Onboarding'.

In "BigData" Sharepoint there are the official Agile Skill courses that must not be shared with external world. This material can be then integrated with other resources in case of need.

# On boarding follow up

<div class="pill">process: buddy</div>

As buddies we never leave the persons we have helped during the on-boarding phase, our accountabilities will always be "sponsoring the company values" and  "helping the people we on-boarded".

The on-boarding follow up process goal is to measure:

* How many concepts are successfully communicated during the on-boarding phase
* How many concepts are remembered or forgotten by the person we on-boarded
* How many concepts we don't talk about during the on-boarding (but we should)

And understand:

* Why things that are remembered (we should reinforce those)
* Why things are forgotten  (should we stop talking about it? are there any reason?)
* Are there things that are helpful to coaches that we should talk more in the on-boarding phase?
* Are there things that the coaches do not know? (we should remind them of those)

## Process steps

* As soon as you start on-boarding schedule the follow up call in 6 months
* Ask the on-boarded person what they found useful in the 6 months timeframe
* Ask the on-boarded person what they did not find useful in the 6 months timeframe
* Go over the onboarding checklist again and note what the on-boarded persons remember and what they do not rememeber
* Interview the coach and ask the same questions about topics in the on-boarding checklist
* Interview the coach and ask what we should talk more in the on-boarding phase
* Ask the on-boarded person to rate their buddy again with the same on-boarding quiz
