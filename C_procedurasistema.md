# 1. Purpose and Scope

The purpose of this guideline is to adequately describe the Management System, the management of security and protection of corporate information, and to serve as a constant reference in the application and updating of the system itself through careful analysis of risks for the management of Agile Lab's operational continuity.

This guideline and the referenced documents are applicable to contractual situations where it is necessary to ensure confidence in compliance with product requirements and data security, by demonstrating the ability to manage the following activities:

Research, study, design, development, and trade of computer systems, telematics, processing, and computing.
Design and implementation of technological and operational platforms for the management and storage of various types of data, including personal data.
Creation, commercial management, and consulting for "virtual spaces" such as websites, social networking platforms, and forums.

# 2. References

The references considered for the definition of the Quality Management System, Information Security, and Business Continuity Management System are based on the following standards:

ISO 22301:2019 "Security and resilience - Business continuity management systems - Requirements"
ISO 27001:2017 "Information technology - Security techniques - Information security management systems - Requirements"
ISO 9001:2015 "Quality management systems - Requirements"
ISO 9000:2015 "Quality management systems - Fundamentals and vocabulary"
ISO 9004:2009 "Managing for the sustained success of an organization - A quality management approach"
ISO 22300:2018 "Societal security - Terminology"
ISO 22313:2020 "Societal security - Business continuity management systems - Guidance"
ISO 22320:2018 "Societal security - Emergency management - Requirements for incident response"
Guidelines of the Italian Authority for the Supervision of Financial Markets (Consob) on business continuity management of market infrastructures, May 2014
ISO 27002:2013 "Information technology - Security techniques - Code of practice for information security controls"
UNI 31010:2019 "Risk management - Risk assessment techniques"
ISO 27018:2019 "Code of practice for protection of personally identifiable information (PII) in public clouds acting as PII processors"
ISO 27701:2019 "Security techniques - Extension to ISO/IEC 27001 and ISO/IEC 27002 for privacy information management - Requirements and guidelines"
EU Regulation 2016/679 "General Data Protection Regulation" (GDPR)
Legislative Decree 101/2018 "Provisions for the adaptation of national legislation to the provisions of EU Regulation 2016/679"
Legislative Decree 81/2008 "Implementation of Article 1 of Law 3 August 2007, No. 123, on health and safety in the workplace" and subsequent amendments
Legislative Decree 106/2009 "Supplementary and corrective provisions to Legislative Decree 81/2008 on health and safety in the workplace" of 3 August 2009
The complete list of standards/laws/regulations is prepared and kept up-to-date by the Quality Management System Manager, who retains the listed documents and the original copy of this manual in their office.

# 3. Definitions

The terms and definitions provided in the UNI EN ISO 90000 "Quality management systems - Fundamentals and vocabulary," UNI EN ISO/IEC 27000 "Information technology - Security techniques - Information security management systems - Overview and vocabulary," and UNI EN ISO 22301 "Business continuity management systems - Requirements" standards are applicable to this document.

For the purpose of this document, the following terms and definitions apply:

ASSET: Anything that has value to the organization.

AVAILABILITY: The property of being accessible and usable upon request by an authorized entity.

CONFIDENTIALITY: The property that information is not made available or disclosed to unauthorized individuals, entities, or processes.

INFORMATION SECURITY: The preservation of confidentiality, integrity, and availability of information; additionally, other properties such as authenticity, accountability, non-repudiation, and reliability may be involved.

INFORMATION SECURITY EVENT: An identified occurrence related to the state of a system, service, or network indicating a possible security policy violation, a failure of security safeguards, or an unanticipated situation that may impact security.

INFORMATION SECURITY INCIDENT: An event or a series of events that are not intended or expected and have a significant probability of compromising business operations and threatening information security.

INFORMATION SECURITY MANAGEMENT SYSTEM (ISMS): That part of the overall management system, based on a business risk approach, aimed at establishing, implementing, operating, monitoring, reviewing, maintaining, and improving information security.

INTEGRITY: The property concerning the safeguarding of the accuracy and completeness of assets.

RESIDUAL RISK: The remaining risk after risk treatment processes.

RISK ACCEPTANCE: The decision to accept a risk.

RISK ANALYSIS: The systematic use of information to identify causes and estimate risk.

RISK ASSESSMENT: The overall process of risk analysis and risk evaluation.

RISK EVALUATION: The process of comparing the estimated risk against given risk criteria to determine the significance of the risk.

RISK MANAGEMENT: Coordinated activities to direct and control an organization with regard to risk.

RISK TREATMENT: The process of selecting and implementing measures to modify risk.

STATEMENT OF APPLICABILITY (SOA): A documented statement that describes the control objectives and controls relevant and applicable to the organization's ISMS.

BUSINESS CONTINUITY: The capability of an organization to continue the delivery of products and services at acceptable predefined levels during a disruption.

BUSINESS CONTINUITY PLAN: Documented information that guides an organization to respond to a disruption and resume, recover, and restore the delivery of products and services consistent with its business continuity objectives.

DISRUPTION: An expected or unexpected incident that causes an unplanned negative deviation in the planned delivery of products and services according to an organization's objectives.

BUSINESS IMPACT ANALYSIS (BIA): The methodology used to determine the consequences resulting from an event and to evaluate the impact of that event on the organization's operations.

DISASTER RECOVERY (DR): The set of technical and organizational measures taken to ensure the operation of the data processing center and the organization's IT procedures and applications in alternative sites to the primary/production ones, in the face of events that cause or may cause prolonged unavailability.

RISK ASSESSMENT (RA): The analysis to determine the value of the risks associated with the occurrence of an event that may interrupt service delivery.

RISK TREATMENT (RT): The treatment of risk following its identification (RA) in order to contain it within acceptable limits for the organization.

RECOVERY POINT OBJECTIVE (RPO): Indicates the tolerated data loss and represents the maximum time between the production of data and its safeguarding (e.g., through backups), thereby providing a measure of the maximum amount of data that the system can afford to lose due to an unforeseen event.

RECOVERY TIME OBJECTIVE (RTO): Indicates the service recovery time and is the duration within which a critical process or primary Information System must be restored after a disaster or emergency condition (or interruption) to avoid unacceptable consequences.

The following abbreviations are also referenced within the guideline:

System Nomenclature

IMS Integrated Management System
FUR Function Unit Responsible
NC Nonconformity
CAR Corrective Action Request
IM Improvement Plan
IA Internal Audit
QLG Quality Guidelines
OI Operational Instruction
AP Procedure Attachment
IS Information System
SW Software
P Probability
S Severity
R Risk
RA Expected Risk
RR Risk Reassessment
BC Business Continuity
BCM Business Continuity Management
BIA Business Impact Analysis
DR Disaster Recovery
BCP Business Continuity Plan
D Disruption
RA Risk Assessment
RPO Recovery Point Objective
RT Risk Treatment
RTO Recovery Time Objective

# 4.1 Understanding the organization and its context

agilelab (referred to as the "Company") has a strategic mission to provide Big Data and Analytics, Machine Learning, Edge AI, IoT, Low Latency Data Streaming, and Domain-Driven Design services, as described on their website https://www.agilelab.it.

In line with its strategic mission, the Company has planned its organizational structure, which includes the adoption of specific management systems.

Within the adopted management systems, the adoption of an information security management system in compliance with the ISO 27001 standard and ISO 27701 for personal data security is particularly relevant, considering the strategic mission outlined above. By adopting a modern information security management system, the Company aims to achieve the following objectives:

Ensure compliance with security requirements applicable to services provided to customers, also in relation to the adopted quality management system.
Ensure compliance with requirements related to the protection of personal data, including the provisions of Regulation 679/2016 and Legislative Decree 196/03 "privacy" and ISO 27701.
In this perspective, the Company has described in a specific document the information security management system adopted.

Furthermore, the Company has also integrated business continuity compliance into its management system according to the ISO 23001 standard, with the aim of increasing its ability to offer products and services that innovate the target market and generate stable and continuous growth while ensuring operational continuity.

Reference is also made to the "Scope of the information management system" document, which is constantly updated based on changes to services and/or operational locations of agilelab.

4.1.1 Identification of critical processes

agilelab has defined the following critical processes that, in the event of an incident that interrupts normal activity, impact the services provided to customers:

Product/service development and delivery
Employee satisfaction
Quality, process improvement, and change management
Responsibility management
Management of technological tools
4.1.2 Asset inventory of IT resources

The company has identified and documented its resources, including:

PCs (including desktop PCs, tablets, laptops, Macs)
Devices (including smartphones, landline phones, fax machines, radiofrequency terminals, UPS, switches, NAS, cameras, routers, firewalls)
IT services and Cloud services, software
4.1.3 Recovery Time Objectives (RTO)

The Recovery Time Objective (RTO) represents the maximum acceptable time for restoring customer support services without causing an irreparable qualitative service disruption.

The RTO is set at 60 minutes, representing the time required to restore service operation by reactivating telecommunication infrastructure.

The achievement of the RTO involves intermediate steps corresponding to partial recovery of operations to partially meet customer demands. These intermediate steps are as follows:

Within 20 minutes of the disruptive event: use of portable routers to ensure internet connectivity and continued use of PCs (desktop or laptops, depending on the availability of electrical power, including UPS activation) to provide customer support.
Within 60 minutes of the disruptive event: relocation to the secondary site with internet and telephone connectivity and electrical power supply for PCs.
Within 60 minutes of the disruptive event (RTO): restoration of operational capability for service staff from the secondary site.
4.1.4 Required recovery levels for each service (Recovery Point Objective - RPO)

The Recovery Point Objective (RPO) represents the maximum tolerated data loss. It describes the time difference between data generation and its secure backup or copying to the disaster recovery site.

At present, this applies only to the company's cloud environment and a service provided to a specific customer, which directly defines the parameters and has specific contractual commitments. Regarding the backup of the company's IT system, the company is aligned with and relies on the backup policies of the Microsoft provider.

Real-time backups to storage and the cloud occur automatically according to the parameters of MS 365 Business Premium. This ensures that data is constantly archived and updated directly in the cloud.

4.1.5 Identification of crisis scenarios

The conditions that require the activation of business continuity are:

Unavailability of premises due to weather events, flooding, fires, etc.
Unavailability of premises due to:
a. Prolonged power outage
b. Prolonged network unavailability for data transfer (internet)
c. Prolonged unavailability of fixed or mobile telephony network
Unavailability of essential personnel: Massive absence of personnel due to, for example, an influenza epidemic or blocked roads.
Loss of documentation

4.1.6 Classification of incidents

Incident Classification:

Level 1: Low impact on business, resolvable with ordinary interventions, responsibility of the service manager or delegate.
Level 2: Moderate impact on business, partial degradation or interruption of service (up to 25% impact), service still operational but with some slowdown, responsibility of the service manager or delegate.
Level 3: Significant impact on business, degradation or interruption of service (26-55% impact), service still operational but with serious disruptions, managed by the Business Continuity Manager.
Level 4: Very high/catastrophic impact, interruption of service (56-100% impact), activation of Crisis Committee.
Impact on Business:

Level 1: Possible legal actions from a limited number of clients, economic damage below 25% of the risk appetite or maximum acceptable risk, service disruption causing minor damage to the organization's image, minor non-compliance with legal norms or regulations with possible administrative sanctions.
Level 2: Possible legal actions from a limited number of clients, economic damage between 25% and 50% of the risk appetite or maximum acceptable risk, service disruption causing damage to the organization, some stakeholders perceive the service disruption, local media may report the disaster, non-compliance with legal norms or regulations with possible administrative sanctions.
Level 3: Violation of contractual clauses, significant number of legal cases in terms of number or amount, economic damage between 50% and 70% of the risk appetite or maximum acceptable risk, service disruption causing serious damage to the organization's image, a significant number of stakeholders perceive the service disruption, national media may report the disaster, non-compliance with legal norms or regulations with possible administrative and/or criminal penalties.
Level 4: Violation of contractual clauses, possible class actions, economic damage exceeding the risk appetite or maximum acceptable risk, service disruption causing severe damage to the organization's image, stakeholders perceive the service disruption and protests may occur, national or international media is involved, serious non-compliance with legal norms or regulations with penalties, including criminal penalties for executives.
4.2 Understanding the needs and expectations of interested parties

The interested parties can have an impact on agilelab's ability to provide services that meet customer requirements and mandatory applicable requirements. Therefore, agilelab has determined:

The relevant interested parties to the quality and information security management system.
The requirements of these interested parties.
agilelab updates this information to understand and anticipate needs and expectations that affect customer requirements and customer satisfaction.

agilelab considers the following stakeholders in relation to the allocation of diverse, professional, economic, financial, and technological resources towards the development of innovative products and solutions that can increase business opportunities throughout the national territory:

Customers
Management
Public entities (Municipalities, Regions, Public Administration)
Suppliers
Third-party customers (end users of the product)
Employees and collaborators
Development team
Sales team
Regulatory/control bodies
Banking institutions
Any other relevant stakeholder
To satisfy all its customers, the company takes into account customer requests and expectations, as well as feedback received from customers in order to improve and better define the requirements of its products and services.

Suppliers are monitored and constantly evaluated, and specific payment terms have been defined as part of the stakeholder agreement. To satisfy both parties involved, agilelab is committed to consistently respecting these defined agreements.

Employees are considered the core of the company, and therefore, periodically, the management meets with the teams to share the company's performance and strategies.

4.2.2 Legal and regulatory requirements

The organization, through the Data Protection Officer (DPO) and the legal firm of reference, identifies legal and regulatory requirements on a regular basis related to the continuity of its products, services, activities, and resources, as described in Chapter 7 of this guideline.

4.3 Determining the scope of the quality management system

agilelab has established a management system to ensure and improve the quality of its business processes in accordance with the requirements of the ISO 9001 quality management system standard. This system integrates the security of information in compliance with the ISO 27001 standard, data security in compliance with ISO 27701, and the applicable legal requirements.

Starting in 2022, the senior management has decided to adopt the business continuity management system in accordance with ISO 22301 because the topic of operational continuity, understood as a set of activities aimed at minimizing the destructive or harmful effects of an event that has affected an organization or part thereof while ensuring the continuity of activities, must be an integral part of the organization's security processes and policies.

The Integrated Management System (IMS) consists of the set of responsibilities, actions, structures, and resources established to implement the Integrated Policy as effectively and efficiently as possible, while continuously improving.

agilelab implements its Integrated Management System, taking into account what is analyzed and expressed in sections 4.1 and 4.2, in order to define the scope that involves the following products:

Research, study, design, development, and trade of computer systems, telematics, processing, and computation.
Design and implementation of technological and operational platforms for the management and storage of various types of data, including personal data.
Creation, commercial management, and consulting for "virtual spaces" such as websites, social network platforms, forums.
4.4 Quality management system and related processes

4.4.1 General information

This guideline describes the company's system, aiming to:

Communicate the integrated policy defined by the Senior Management to both internal and external stakeholders of agilelab.
Provide all personnel involved in quality-relevant activities with a tool for achieving and maintaining the predetermined quality level.
Address risks and opportunities.
Promote prevention of service disruptions and create synergies that foster continuous improvement of customer service.
Define the scope of the quality and information security management system.
Describe the processes and interactions within the integrated management system.
Reference the necessary documents for the effective management of the processes.
Define roles and responsibilities within the processes and the integrated management system.
Maintain appropriate documented information as evidence of the functioning of the processes and retain this documented information to ensure that processes are managed as planned.
The quality management system documentation includes:

Policy and objectives
Guidelines
Operating instructions
Documents deemed necessary to ensure effective planning, operation, and control of its processes
Plans and tests
Relevant records
4.4.2 Process approach

In addition to the introductory chapters (Chapter 0 to the present Chapter 4), this guideline consists of six additional sections that describe the macro processes of the quality management system according to the "Process Model" of the standard.

Each section comprehensively covers topics related to the normative reference.

The identified processes (as shown in the company's macro-process flow) are controlled through a set of indicators specifically identified for measuring the effectiveness and efficiency of these processes.

These indicators are issued and kept up-to-date by the Senior Management of agilelab, who, during the review process, evaluates the need to develop improvement plans.

If planned results are not achieved, it is the responsibility of the Senior Management to take corrective actions.

The organization maintains appropriate documented information as evidence of the functioning of its processes and retains such documented information to ensure that the processes are managed as planned.

Documented information related to the Quality Management System is listed in the "System Document List" module.

agilelab uses external suppliers to support its production process (outsourced processes).

To regulate these relationships and ensure control, agilelab has defined methods for supplier control, including:

Supplier qualification and monitoring activities.
Acceptance controls of products.
Audits at suppliers' premises.
These controls aim to verify that the supplier complies with the correct execution of commissioned activities. The responsibility of the Senior Management includes communicating and transmitting the specifications of the requested products/services to the supplier, as well as monitoring the service execution and managing any non-conformities identified in relation to the supplying company.

# 5 Leadership

5.1 Leadership and Commitment

Top management must demonstrate leadership and commitment to the quality management system.

5.1.1 General

The top management of agilelab promotes and takes personal responsibility for quality in all functions, products, and services of the organization, with customer satisfaction as the primary objective. They are responsible for:

Understanding customer and mandatory requirements and communicating them within the company. Defining and disseminating a Quality Policy and Information Security and Operational Continuity document consistent with these requirements.
Communicating the Policy and the importance of correct implementation of the integrated management system.
Promoting a culture of quality accompanied by awareness of the need to apply the integrated management system.
Promoting the use of a process approach and risk-based thinking.
Defining objectives to monitor the performance of the integrated management system.
Ensuring the availability of necessary human and material resources.
Establishing an integrated management system that controls the achievement of set objectives and stimulates continuous improvement.
Periodically reviewing the integrated management system in relation to its application and effectiveness.
The expectations for the required products and services are understandably high and can only be achieved if they are understood. In this regard, the following measures are necessary:

Involvement of people at all levels.
Process approach in resource and activity management.
System approach in process management, focusing on objectives.
Risk-based thinking.
Continuous improvement.
Data-driven decision-making.
Measurement of customer satisfaction through indicators.
5.1.2 Customer Focus

The top management's objective is customer satisfaction, and therefore, customer requirements are constantly monitored using appropriate indicators.

The level of customer satisfaction is evaluated using specific methods described in Chapter 9 of this guideline, taking into account any complaints or specific requests from customers. As customer satisfaction is the company's primary objective, sales personnel are responsible for reporting any requests or anomalies presented by customers in any form to agilelab.

To meet customer requirements, agilelab evaluates the risks and opportunities that the company may be exposed to, as described in Section 6.1 of this guideline.

The expectations for the required products and services are understandably high and can only be achieved if they are understood.

To ensure this, it is necessary to:

Involve people at all levels.
Adopt a process approach in resource and activity management.
Adopt a system approach in process management, focusing on objectives.
Pursue continuous improvement.
Base decision-making on factual data.
Measure customer satisfaction through indicators.
5.2 Policy

5.2.1 Establishing the Quality and Information Security Policy

agilelab has defined its own Policy that outlines a direction to follow in order to always ensure a future according to its expectations.

The Policy, as an expression of the management's commitment to the development and implementation of the integrated management system, is issued and kept up-to-date by top management.

agilelab conducts systematic reviews of its policy, objectives, and indicators on an annual basis to ensure their effectiveness in a constantly changing market.

In defining, issuing, and updating the Policy, the top management pays particular attention to:

Achieving and maintaining the quality of its products and continuously improving them.
Improving the quality of its operations to continuously meet the expressed and implied needs of customers and other interested parties.
Preventing potential risks to information security.
Defining the needs of customers and other interested parties in order to meet their requirements and achieve continuous improvements.
Ensuring that the Business Continuity Management System achieves the expected results.
Facilitating active participation and guiding and supporting people to contribute to the effectiveness of the Business Continuity Management System.
Compliance with current legislation/regulations.
Preventing/minimizing occupational risks.
5.2.2 Communicating the Policy

RSQ identifies the most appropriate channels to disseminate the Policy document, taking into account that the policy should be communicated to all individuals working for or on behalf of the organization.

The Policy is reviewed annually by top management and managed by RSQ, who ensures its transfer to the Guidelines and communicates it to all individuals working under the organization's control and distributes it to various business areas.

The Policy is made available to all company functions and is discussed by top management together with the staff once a year during a company meeting.

agilelab communicates its policy to stakeholders through the website at the following address: www.agilecompliance.website, as well as during events, introduction letters, and any other suitable means.

5.3 Roles, Responsibilities, and Authorities in the Organization

Within agilelab, there are various "roles" and "working groups" called "circles" that are associated with a set of objectives, domains, and responsibilities. Within this framework, individuals assigned to these roles and working groups have autonomy and delegated authority.

The definition of roles and circles and their attributes is outlined in the Handbook, available at the link https://handbook.agilelab.it/BusinessUnit.html, which is public and periodically updated to adapt the organization to the company's needs. The handbook aims to make explicit the company's rules and functions and define expectations for each role or circle.

Within a circle, there may be sub-circles and roles, and each circle has a leader who organizes the circle independently.

The leader of each circle is responsible for reporting any activities that cannot be addressed autonomously during meetings to the higher-level circle so that they can be addressed in a structured and organizational manner.

The following are the main circles, which may contain sub-circles:

HR
Crisis committee
Finance
PeopleOps
Sales
Engine
Products
Aim2
SRE
Internal IT
Software Factory
Architecture
AgileSkill - training
Marketing
During the system review, the top management is also responsible for ensuring the availability of resources.

With reference to the above organizational chart, the main responsibilities and authorities of the functions that influence quality and information security are defined as follows. For more details on other roles, please refer to the information provided in the handbook at the link mentioned.

To ensure that the provisions contained in this Guideline are applied and maintained, the top management has assigned responsibilities within the organization.

To the Project Quality Representatives (RQ), they assign the responsibility and authority to:

Ensure that the Quality and Information Security Management System complies with the requirements of these international standards.
Report, particularly to top management, on the performance of the integrated management system and improvement opportunities.
Ensure the integrity of the integrated management system is maintained when changes are planned and implemented.
To the IT Support Manager (Responsabile IT), for the field of information security, they have assigned responsibilities and delegated the management of the following activities:

Risk management
Information security governance
Compliance with regulations
Business continuity and disaster recovery
Privacy management
To the Business Continuity Manager (RCO), they have assigned the responsibility to:

Contact all members of the Crisis Management Committee for regular meetings.
Conduct a review of the Business Continuity Plan.
Supervise emergency drills.
In case of a disaster/emergency declaration, prepare a report outlining the phases and evolution of the emergency, which will be sent, if required, to the affected client(s) involved in the activation of the BC and DR Plan.
Top Management (DG - Direzione Generale):

Represents the Company from a legal and administrative standpoint and establishes the company's business and commercial strategies.
Is responsible for organizing the company, defining, disseminating, and achieving objectives, promoting and selling the product, ensuring customer satisfaction, controlling the established budget, coordinating the various functions that make up the company, containing costs, and increasing the company's profit.
Approves the system documentation, authorizes its issuance, and requires compliance from all involved structures.
Promotes and takes personal responsibility for quality in all functions, products, and services of the company.
Is responsible for defining, updating, and disseminating the quality policy, including objectives and commitments.
Is responsible for defining the responsibilities, authority, and mutual relationships of the personnel they lead, perform, and verify activities that influence quality.
Is responsible for systematically reviewing the management.
Is primarily responsible for identifying areas of potential improvement and taking appropriate actions.
Is responsible for defining customer activities.
Is responsible for providing the company with adequate resources necessary for the operation of the system and continuous improvement in terms of customer satisfaction and information security.
Is responsible for defining and approving training plans for personnel and approving internal functional roles.
Is responsible for defining roles for personnel management.
Manages Business Intelligence related to customers and competitors, shares any identified critical issues regarding BI with internal stakeholders.
Has the responsibility to appoint individual Project Managers for various Business Units and Calls for Proposals. Evaluates the performance of Project Managers by assessing project completion times and the quality of the final results.
Quality Manager (RQ - Responsabile Qualità):

Is responsible for the management of the system documentation, particularly in terms of preparation, distribution, archiving, and updates.
Is responsible for collaborating in the management of supplier qualifications.
Is responsible for monitoring supplier performance.
Is responsible for defining, in collaboration with top management, training and development plans for all personnel. Coordinates the planning, execution, and recording of training and development activities.
Is responsible for defining control specifications, in collaboration with other functional managers, for materials in the acceptance, production, and release phases.
Supports the functions responsible for managing product non-conformities and analyzes any system non-conformities or emerging trends to identify their causes. Is responsible for managing corrective and preventive actions and supports top management in pursuing continuous improvement.
Is responsible for managing complaints and non-conforming products.
Plans and conducts (or ensures the conduct of) internal audits in collaboration with top management to assess the efficiency and effectiveness of the quality management system and takes specific corrective actions and improvement plans.
Regularly reports to the Management Representative on the performance of the management system.
Collaborates with top management for system review activities, including measuring customer satisfaction.
Is responsible for keeping appropriate statistical indices up-to-date and providing them to top management.
IT Support Manager (RSI - Responsabile Supporto Informatico/Responsabile IT):

Is responsible for the design, programming, development, and management of all hardware, software, mobile telephony, and data transmission systems of the company.
Is responsible for applying and ensuring compliance with policies and instructions for the management of the information security system in accordance with ISO/IEC 27001.
Maintains relationships with software and hardware suppliers for all company programs and collects and coordinates the needs of various departments for program implementations, including applications.
Is responsible for the company's backup system and may delegate operational responsibilities to suitable collaborators.
Manages the company's IT systems, particularly focusing on new technologies for the development of company products.
Coordinates and promotes the activities of all company personnel involved in IT system management.
Collaborates with management in managing IT-related supplier contracts and analyzing the company's IT needs.
Collaborates with management to verify the compliance of supplies with the contracts entered into in the IT field and detects any deviations.
Ensures the proper functioning of adopted systems.
Regularly consults the heads of departments regarding their IT needs, including training courses.
Proposes any necessary solutions, new IT requirements, implementation needs, and necessary measures for the management and maintenance of the company's IT system.
Assists staff involved in implementing company IT systems and verifies any issues reported by the staff responsible for using the systems.
Verifies the results achieved by the applications of IT systems by individual departments/areas.
Manages the company's IT security activities (access profiling to the network, adoption of appropriate measures for defense against external attacks, etc.).
Sets up and manages an IT authentication system for processing personal data through electronic means.
Actively collaborates with the Data Controller to constantly verify that the company adopts appropriate security measures in accordance with EU Regulation 2016/679 and 27701, promptly reporting any necessary adjustments.
Suggests to the Data Controller the adoption and updating of the most appropriate security measures to meet the requirements of Article 32 of EU Regulation 2016/679, including, among others, if necessary:
Pseudonymization and encryption of personal data.
The ability to ensure, on an ongoing basis, the confidentiality, integrity, availability, and resilience of systems and processing services.
The ability to promptly restore the availability and access to personal data in the event of a physical or technical incident.
A procedure for regularly testing, verifying, and evaluating the effectiveness of technical and organizational measures to ensure the security of processing.
Ensures, with the delegation of top management, the adoption and updating of "adequate" measures mentioned above, assisted by the person responsible for managing activities on the operating systems.
Activates and updates, at least every six months or according to suitable measures identified by the company, appropriate electronic tools to protect the data processed through the company's IT systems from the risk of intrusion and the action of computer viruses, updating programs accordingly to prevent vulnerabilities and correct defects.
Provides PMA (Project Management Authority) activities when delegated by top management.
Crisis Management Committee (CGC - Comitato di gestione della crisi):

The Crisis Management Committee is the top-level body in crisis management, responsible for making key decisions and overseeing the activities of the involved resources. It is the strategic management body of the entire structure in the event of a crisis and is responsible for guaranteeing and controlling the implementation of the BC and DR Plan.

The decisions of the Committee will be documented in a specific report once the emergency is concluded and countersigned by the Business Continuity Manager or the Deputy Business Continuity Manager. The report should contain all the information regarding the activation of the business continuity process, the declaration of recovery from the emergency, which is the responsibility of the Crisis Management Committee, and the necessary decisions to return to normal.

The Crisis Management Committee of the Company is composed of the following positions:

Business Continuity Manager (RCO)
Customer Support Manager
IT Manager
Legal representative of the Company
In case of a disaster/emergency declaration, the Business Continuity Manager contacts all members of the Crisis Management Committee.

The Crisis Management Committee is responsible for:

Defining, approving, and updating the Business Continuity Plan.
Evaluating emergency situations and declaring a state of crisis.
Initiating and controlling recovery activities.
Communication with external parties and employee communications.
Initiating activities to return to normal conditions and controlling their implementation.
Declaration of recovery.
Managing all unforeseen situations necessary for the proper implementation of the BCP.
Promoting and coordinating training and awareness activities regarding business continuity.
In the event of a disastrous incident, the Committee assumes control of all operations and takes responsibility for decisions to address the emergency, reduce its impact, and restore pre-existing conditions.

In the event of a severe incident, the Crisis Management Committee Leader may decide to delegate the coordination of operations to the Crisis Management Committee or handle it autonomously.

The Committee must be supported by other figures within the company and by personnel, if necessary, to ensure the functioning of the BCP regarding the following activities:

Support for any required movements.
Ensuring the functioning and access to all IT and telecommunications infrastructure.
Updating related information from public communication channels.
Examining all security aspects, particularly regarding the security level provided by configurations adopted for the emergency and data protection, either through a review of solutions adopted for system recovery and return to normalcy.
I hope this information clarifies the roles, responsibilities, and authorities within the agilelab organization.

# 6 Planning

6.1 Actions to address risks and opportunities.

In planning the quality management system, information security, and business continuity, the organization has considered the issues referred to in clause 4.1 and the requirements referred to in clause 4.2, and has identified the risks and opportunities that need to be addressed in order to:

ü Ensure that the quality management system and information security can achieve the intended results;

ü Ensure that the organization can consistently achieve service compliance and customer satisfaction;

ü Prevent or reduce undesired effects;

ü Achieve improvement.

agilelab plans appropriate actions to address risks and opportunities and defines the methods to:

ü Integrate and implement the actions within its quality management and information security processes (see clause 4.4);

ü Evaluate the effectiveness of these actions.

Every action taken to address risks and opportunities must be proportionate to its potential impact on the conformity of goods and services and customer satisfaction.

agilelab establishes three types of planning:

• Strategic planning, which includes overall choices related to the management system, the preparation and publication of the integrated policy, the selection of strategic objectives, the identification of processes and controls within the management system, their general characteristics, and their relationships;

• Tactical planning, which establishes the details of the processes and security controls to be implemented, their objectives, the resources needed to achieve the planned activities, the activities that make up the processes, and their frequency or deadline;

• Operational planning, which determines when to perform activities such as facility maintenance, user reviews, execution of recovery tests, execution of business continuity tests, execution of vulnerability assessments, acquisition of material resources, personnel recruitment, formalization of contracts with suppliers.

The Management believes it is appropriate to assess risks through an evaluation carried out in conjunction with the various Business Units in order to identify all possible critical aspects and opportunities for the company. Furthermore, the Management believes that by involving all functions in this process, they will be sensitized to act according to Risk-Based Thinking.

Risk R is the product of two factors PxD, where the two factors are assigned numerical values ranging from 1 to 5, according to the Probability-Severity Matrix.

Evaluation of risks and opportunities for quality

Following the evaluation, all identified risks are assigned a numerical value between 1 and 25. If the risk value is between 1 and 6, it falls within the green range and is therefore acceptable, so it is not addressed because it is not believed to have an impact on the company. If the risk value is between 12 and 25, it falls within the red range, it has priority, and it is addressed by opening specific improvement plans. Risks with a value between 9 and 11 are placed in the yellow range, indicating a medium risk.

These risks do not pose an immediate risk to the company, but they must be monitored, and therefore specific indicators will be opened. For risks in the red range, actions should be taken as soon as possible.

The monitoring of results is carried out through Improvement Plans, and in case of non-conformities, corrective actions are defined when necessary.

During the risk analysis, the company also defines, for each assessed risk, the value of Expected Risk (RA), which can always have a value between 1 and 25 like R, and represents the company's expectation for that specific aspect considered.

agilelab constantly reviews the performance of the integrated management system, analyzes the evolving market and context in which it operates, together with the indicators and outputs of the processes, using them as input elements for the evaluation of risks and business opportunities in order to define appropriate corrective actions and improvement plans to minimize risks arising from identified critical situations.

Annually, the Management, based on the data and measurements collected during the year, reanalyzes the risk levels present in the company, involving the various Business Units and using the data collected on the performance of the Management System.

During the update of the assessment, the Management uses the collected data and events occurred, modifying the probability of occurrence, according to the data defined in the probability-severity matrix.

Furthermore, the Management reserves the right to update the assessment and open improvement plans and/or corrective actions when necessary due to changes in the system (hiring, new machinery, new processes, incidents, data loss) or whenever deemed necessary.

The verification of the actions taken, as well as the status of ongoing improvement plans, is periodically carried out by the Management in collaboration with the relevant Business Units, and the effectiveness of the action taken is assessed during the management review by the Management with the Quality and Safety Responsible.

6.2 Objectives for quality, security, business continuity, and planning for their achievement.

The organization's integrated management system consists of the Policy, the organizational structure with defined responsibilities, the procedures, and the resources implemented for quality management, information security policies to commit to achieving objectives (effectiveness) with the least expenditure of energy (efficiency), and policies for business continuity.

agilelab has defined and keeps updated specific objectives aimed at improving the organization's performance.

In accordance with the Policy statement, as well as the risk analysis and the reference context, the DR establishes objectives annually based on appropriate indicators.

These objectives are formalized through specific documentation during the review and communicated to the responsible parties through planning and conducting specific meetings, adequately documented, held by the DIR with the support of the RSQ.

They are evaluated and reviewed by the Management, and the outcome is shared and discussed with the various Business Units.

The objectives are defined and reviewed based on:

Compliance with current regulatory and legal requirements;

The policy;

Results of internal audits;

Company indicators;

Complaints and/or feedback from interested parties;

Risk analysis;

Incidents.

and taking into account:

Current and future organizational needs.

Results of Management review.

Performance of its processes and products.

Customer satisfaction.

Necessary resources.

The organization has defined appropriate company indicators and the use of appropriate statistical methods to monitor and numerically measure the performance of the Management System in accordance with the company's policy and defined objectives.

The main indicators include:

Measurement of company processes.

Measurement of system performance.

Analysis of non-conformities.

Measurement of customer satisfaction.

Measurement of policy and objectives.

The company defines the methods to achieve the objectives, the timing, as well as the responsibilities, resources allocated to them, and the methods by which they will be evaluated to verify their achievement.

The achievement of objectives is monitored through appropriate indicators, formalized through specific forms. These indicators not only "measure" the objectives resulting from the "policy," but also measure internal processes, providing essential feedback information to the Business Units for the management of the organization.

For information security

agilelab establishes objectives related to the functions, levels, and relevant processes necessary for the Information Security Management System.

The management of agilelab ensures that objectives related to the Data Security Management System are established for the relevant company functions and levels.

The general principles of information security management encompass various aspects:

- There must be a constantly updated list of relevant company assets for information management, and for each asset, a responsible person must be identified. Information must be classified based on its criticality level in order to be managed with consistent and appropriate levels of confidentiality and integrity.

- To ensure information security, every access to systems must undergo an identification and authentication procedure. Access authorizations to information must be differentiated based on the roles and responsibilities of individuals so that each user can access only the information they need, and access authorizations must be periodically reviewed.

- Procedures must be defined for the secure use of company assets, information, and their management systems.

- Full awareness of information security issues must be encouraged among all personnel (employees and collaborators) from the selection process and throughout the duration of the employment relationship.

- To be able to manage incidents promptly, everyone must report any security-related problems. Each incident must be managed as indicated in the procedures.

- The access to company premises and individual facilities where information is managed must be prevented from unauthorized access, and the security of equipment must be ensured.

- Compliance with legal requirements and principles related to information security must be ensured in contracts with third parties.

- Security aspects must be included in all phases of design, development, operation, maintenance, assistance, and disposal of information systems and services.

- Compliance with legal, statutory, regulatory, or contractual requirements and any requirements related to information security must be ensured, minimizing the risk of legal or administrative sanctions, significant losses, or damage to the company's reputation.

The definition of Objectives and their review usually takes place following a review of the contents of the company's policy, the analysis of current national and, where applicable, international regulations, the analysis of the contents of the risk assessment document, as well as the results of internal and external audits conducted by third parties.

If a project concerns new developments or new or modified activities, products, or services, programs must be reviewed, if necessary, to ensure that a proper Information Security System is applied to them.

For Business Continuity

To ensure Business Continuity, it is necessary to establish a system that includes logistical, organizational, and technological solutions (Disaster Recovery). This system must be capable of effectively and promptly supporting the organization in emergency situations.

The company's Management defines and plans Business Continuity objectives in agreement with the various responsible functions.

Business Continuity objectives are formalized in the respective Policy, the Management Review, and Improvement Plans.

The principles that establish priorities in emergency/crisis management and guide decisions are:

- Protecting the lives and safety of individuals;

- Preventing further consequences arising from the original incident;

- Protecting business continuity and safeguarding the company's image;

- Cooperating to ensure the continuity of the credit-financial system;

- Safeguarding assets for which the company has responsibility or availability, while respecting the environment.

agilelab has prepared a continuity plan that allows the company to effectively address unforeseen events, ensuring the restoration of critical services within timeframes and through methods that limit the negative consequences on the company's mission.

6.3 Information security risk treatment

agilelab determines and applies a risk treatment process to:

a) Identify options for risk treatment, taking into account the results of risk assessment;

b) Determine all necessary controls to implement the selected options for information security risk treatment;

c) Compare the identified controls with those in Appendix A of the standard and verify that no necessary controls are omitted;

d) Prepare the "Statement of Applicability" document, which lists the necessary controls and justifies the inclusion of certain controls;

e) Formulate a risk treatment plan;

f) Obtain approval of the risk treatment plan and acceptance of residual risks related to information security by the risk owners.

The company has established an instruction for the treatment of information security risk called IO 06.2 Information Security Risk Treatment.

The organization implements the information security risk treatment plan, which is periodically reviewed according to the following frequency:

Annually as a result of management review, in the absence of non-conformities;

In the event of non-conformities;

In the event of significant changes in company assets;

In the event of significant changes in the context (internal/external context).

6.4 Business continuity risk treatment

agilelab determines and applies a risk treatment process through the implementation and updating of the Business Continuity Plan through scheduled testing.

The operational procedures are specified in IO 10 - IT Business Continuity Plan.

6.5 Change planning

It is the responsibility of the Quality System Manager to ensure the adaptation of the system to the company's reality and needs over time (changes), taking care to preserve the integrity of the system itself and collaborating with the IT Manager to define the necessary measures for their control and with the Business Continuity Manager.

agilelab determines the needs and opportunities for change to maintain and improve the performance of the management system, following the review of the company's situation, context, processes, and resources involved.

agilelab has undertaken change in a planned and systematic manner, identifying risks and opportunities and reviewing the potential consequences of change.

The company's Management meets periodically to analyze the progress and define any necessary changes or plan changes to the system that they deem necessary to ensure the quality of their products and services, information security, customer satisfaction, compliance with applicable regulations, and ensuring an acceptable level of business continuity even in the event of adverse events.

In the event of any changes deemed necessary by the Management or the various Business Units, the RSQ opens a specific improvement plan, as defined in Chapter 10 of this guideline. Additionally, the context analysis is updated and the risk assessment is reviewed to assess any opportunities and new risks associated with the ongoing changes.

# 7 Support.

7 Supporto.

7.1 Resources

To maintain and improve its quality management system, information security, ensure the achievement of set objectives, and increase customer satisfaction for all other parties involved in the management of activities, while respecting commitments and ensuring operational continuity, the Management has planned and made available all necessary resources, including human, infrastructure, technological, and informational resources.

7.1.1 General

agilelab has considered the following in planning its resources:

a. What existing resources, capabilities, and limitations are present internally;

b. What resources and competences need to be sourced externally;

c. What goods and services need to be procured externally.

The Management constantly monitors resources in terms of personnel and infrastructure, in relation to workload and potential workload variations, as well as the development of new projects/products/services.

To ensure high-quality levels and achieve set objectives without compromising the expectations of stakeholders or the level of product and service offered, the Management outsources certain processes to maintain a constant focus on processes directly involved in service delivery.

7.1.2 People

agilelab ensures adequate personnel for the implementation of the management system to meet customer demands and ensure compliance with applicable regulations.

Each individual may be assigned multiple roles, and each role may be filled by multiple individuals. A role has defined responsibilities and purpose. Roles will be created and modified based on the current needs of the company to achieve the best results, generating extreme flexibility in the organization.

The company has established personnel management aimed at:

Determining the necessary competencies for personnel performing activities influencing the quality of the product and service.
Providing training.
Ensuring the organization is capable of meeting the required requirements.
Verifying the effectiveness of actions taken.
Ensuring involvement and awareness of personnel.
Maintaining appropriate records related to the activities described below.
For this purpose, an operational instruction (IO 07 Ges Ris - Human Resources) has been prepared, which is referred to in addition to the public Handbook, which provides details on the definition and implementation methods mentioned above [link provided].

7.1.3 Infrastructure

The Management is committed to determining, providing, and maintaining infrastructure for process implementation to ensure service compliance. Infrastructure includes buildings, equipment, and information technology.

During operational phases, function managers express the needs for acquisition or maintenance of the necessary equipment for their activities.

The RSPP is responsible for periodically verifying the work environment to improve organizational performance.

MAN performs planned maintenance checks and interventions (preventive and routine). This maintenance can be performed by MAN assisted by ADD or external suppliers (with whom service contracts can be established) and is carried out periodically for each individual machine according to the established frequencies in the planning.

All mobile phones and personal computers of the Company are monitored and maintained remotely through the adoption of the Microsoft Endpoint Manager service (formerly "Intune").

Each device is monitored by the Microsoft Endpoint Manager control panel, where all relevant events are recorded.

There has never been a need for external companies to manage the company's infrastructure.

Another type of intervention is extraordinary (or in case of breakage) when an unforeseen anomaly occurs. This maintenance can be performed by MAN assisted by ADD or external suppliers (with whom service contracts can be established). MAN records it on the designated form for each machine/equipment, indicating:

Maintenance date.
Description of the intervention and replaced parts.
If applicable: hours dedicated, who performed them, and the cost.
Any maintenance reports from external companies are also attached.

These cards can also be managed and stored electronically/file format.

In case of accidental damage, the employee using the equipment must inform CQ, which will then proceed, after verification, to carry out or allocate the equipment for an extraordinary "inspection."

If the equipment needs to remain idle under specified conditions awaiting inspection, it must be identified as unusable/non-compliant.

In case of Non-Conformity detected during the verification operations, it is the responsibility of CQ to potentially prepare a "Non-Conformity Report" in reference to company equipment or manage it through non-conformity resolution:

Restoration internally or by the supplier.
Scrapping.
Downgrading.
In the event of Non-Conformity related to equipment during use, it is the responsibility of users (CQ/ADD) to assess the need to verify the reliability of previous verifications performed with the equipment and take appropriate measures related to the processed product.

ASSET Management

Asset management activities are carried out through MS Endpoint Manager.

Endpoint Manager protects, distributes, and manages all users, apps, and endpoint devices without interrupting existing processes.

Equipment (PCs and smartphones) is encoded with their model identifier to ensure unique identification.

In the monthly plan, the corresponding code for the interventions to be carried out is inserted, based on the established frequency, which corresponds to a legend for the interventions.

The intervention frequencies can be modified based on equipment/machinery wear conditions, the experience of the maintainer, and evidence from previous interventions.

An annual review of intervention frequencies is conducted, and a new plan is issued.

7.1.4 Environment for Process Operation

Infrastructure/Work Environment

agilelab has suitable and properly equipped locations for carrying out its activities.

The Management continually monitors the suitability of infrastructure (with particular attention to process equipment and communication and information systems) and the work environment, ensuring they do not affect the organization's ability to comply with product and service requirements.

Furthermore, the Management takes into account the requests of various company functions to keep the situation regarding infrastructure and work environment constantly monitored and ensure their compliance based on the work to be performed.

The Management directly and constantly communicates with its personnel at all levels to monitor the level of satisfaction/dissatisfaction of various functions because it believes that a better work environment generates better results in terms of productivity.

The spaces and environments are managed by agilelab in compliance with applicable regulations (Legislative Decree 81/08), protecting the health and safety of personnel and ensuring that the work environment is suitable for the organization's purposes.

# 8 Operational Planning and Controls

The planning for product and service realization, and the operational controls, are reflected in this guideline and its associated documentation. In cases where such documentation does not fully cover particular products and services, the organization will issue dedicated quality documents.

In planning for product and service realization, the company has defined:

@ The objectives for quality and the requirements related to the product and service;

@ The need to establish processes and documents, and to provide specific resources for the product and service;

@ The required verification, validation, monitoring, inspection, and testing activities specific to the product/service and the related acceptance criteria;

@ The records necessary to demonstrate that the processes and resulting products and services meet the requirements.

The company also controls processes entrusted to external entities, as defined in this chapter.

For information security

Agilelab plans, implements, and controls the processes necessary to meet information security requirements and to take the necessary actions to address risks. Additionally, the organization implements plans to achieve the objectives for information security.

In particular, the company has defined specific documents for operational control resulting from the application or non-application of the controls provided for in Annex A in the excel document Risk Analysis and SOA

And defining the ways of managing risks in IO 6.3 Information Security Risk Treatment.

The company manages and monitors the actions taken, assessing their effectiveness and managing non-compliances and corrective actions.

8.2 Requirements for products and services

8.2.1 Communication with the customer

The organization has defined specific methods for communicating with the customer regarding:

@ Information about the product and service;

@ Contract management and related changes;

@ Design status and order status;

@ Feedback information on products and services;

@ Forwarding complaints;

@ Requests for information and/or various communications.

8.2.2 Determination of product and service requirements

Adequate procedures have been prepared in order to ensure the adequacy and correctness of the data and information assumed as a basis for service delivery through the review and coordination of the documents produced by Agilelab and/or indicated by customers. The objective is to ensure that contracts are completed, satisfying all internal and external needs, contractual requirements, as well as applicable binding constraints.

At this stage, an evaluation of the compatibility of the request with the company's technical and productive capabilities (feasibility analysis) is carried out.

This analysis is carried out by the Commercial/COM function manager, if necessary, involving the various company functions needed for aspects of their competence.

COM verifies that:

· all contractual and technical requirements are adequately documented;

· the company has the necessary capabilities.

From a careful examination of the information received from the Customer, checking that all requirements, specifications, standards, specific customer operating instructions, and other necessary data are present, doubts may arise, in which case COM activates to resolve them immediately with the Customer or in the case of alternative solutions and improvements, proposes them directly in the offer, leaving the Customer with the choice in defining the order/contract.

The positive outcome of the feasibility analysis leads to the definition of the offer through the sending of an email for the offer, filled in for the definition of the data relating to the processing offered, price, and commercial specifications.

All further responsibilities for carrying out checks, recording outcomes, and archiving documentation are defined and specified in the following sections of this guideline.

8.2.3 Review of product and service requirements

To verify that the requirements (of the customer, binding, of the company itself) are adequately defined and documented, that the company is able to meet them, and that any deviations between what is indicated in the contractual documents and what is proposed during project approval are resolved, Agilelab carries out a review of the offer.

If all feasibility requirements are met, COM proceeds with the preparation of the offer, taking into account all identified conditions and expected timings, subsequently an email is prepared and sent.

The signing of the offer or the writing and sending of the email (even if accompanying) constitutes evidence of review of the same, to ensure that it fully meets the Customer's request and that it contains all the necessary data. The offer is then sent to the Customer, while the original or a copy of it is archived together with the request by COM.

COM manages a List of offers to customers electronically to maintain significant data and the progressive numbering of issued offers and to mark which offers have been acquired through a Customer's order.

The Customer, having evaluated the offer, in a positive case, prepares the order/contract to be sent to Agilelab, they may request an order confirmation.

If the Customer does not accept the offer and does not communicate it, the offer remains archived for a short period and then deleted, while if they communicate the refusal verbally it is marked on the offer. Unaccepted offers are kept, at least for the current year, as a historical record on which to base business analyses.

In case of changes to be made, one returns to a previous point, depending on the type of change to be made.

Once the final order from the Customer is received, COM will proceed to the final phase of contract review by verifying that the offer and order (contract) requirements match and therefore that there are no deviations.

In the event that what is reported does not comply with the conditions mentioned in the offer, such problems will be resolved by the same Commercial function and annotated on the Offer or Customer's Order.

In case of a negative outcome of the review, one returns to one of the previous points, until the final fine-tuning of the contract annotating the problems and resolution or refusal of the order (contract).

Particular attention should be paid to the evidence of regulatory references, technical specifications, operating instructions referred to by customers on their contractual and technical documentation in order to verify their availability in the company and/or the request for acquisition from the customer.

The process is identified in the Handbook at the link: https://handbook.agilelab.it/ProjectAndProductDelivery.html

Agilelab produces documents that specify the characteristics and structure of each service or product offered and provide information that allows identifying the correspondence of what is proposed to the needs of customers in terms of:

· type of intervention;

· description of the needs to be met;

· duration;

· data security guarantees;

· technical and documentary support;

· economic and contractual elements;

· any logistical support offered;

· post-sales technical support.

8.2.3 Changes to product and service requirements.

Any changes to the requirements for agilelab's products or services are appropriately communicated during the review phase of the order to the customer or of the documentation in use at the sales office.

Regarding any changes, it is necessary to distinguish the following types:

· those requested by the Customer after the order has been acquired, for which a new review of the contract will be carried out based on the new documentation received from the Customer and, if positive, the changes that have occurred will be communicated to the interested functions.

· those requested by agilelab b to the Customer after the order has been acquired, for which the Customer's approval will be requested based on any modified documentation received from the same Customer, and the changes that have occurred will be communicated to the interested functions.

Changes are managed by the same corporate functions responsible for the activity described in the procedure, they are communicated to the interested functions and there is provision to clearly identify the outdated contractual documentation.

8.3 Design and development of products and services.

8.3.1 Generalities

agilelab defines, implements, and controls the design and development processes. Particular attention is paid to defining the input elements that influence the design and development process. These

come from data from customer requests or market demands, from the entry into force of binding standards that require the design of changes to existing products and services.

Planning activity is carried out, followed by operational design phases. The correspondence between what is defined in the planning phase and what is actually carried out is verified through verification, review, and validation activities.

Given the type of service/product from agilelab, the design coincides with the planning and development activities of production, below are generally mentioned the aspects concerning the design process, for the detail please refer to chapter 8.5 Production and service delivery.

https://handbook.agilelab.it/ProjectAndProductDelivery.html

8.3.2 Planning for design and development

The agilelab production process is mainly based on:

• the execution of projects, which involve the functional and technical design of software applications based on the requirements of the Client and the related realization (or development in the strict sense);

• the provision of existing application maintenance services, which involve a series of functional and technical design interventions and the realization of corrective, adaptive, perceptual or evolutionary changes to these applications, depending on the scope agreed contractually with the Client;

• the realization and maintenance of software products, which involve the initial design and realization of the products that agilelab develops on its own and freely proposes on the market and their subsequent maintenance over time. The design, realization, and maintenance phases are managed in agilelab as internal projects and therefore, in the following, the concepts expressed for the projects apply, within the Quality System, analogously to the products.

agilelab pursues an “Agile” approach to project planning and implementation, thus organized according to successive sprints or iterations with presentation of intermediate results at the end of each sprint.

For each project or service, a Project Owner is appointed who is entrusted with the responsibility of achieving specific objectives, in accordance with the general ones indicated by Management, namely:

• respect for deadlines;

• respect for the budget;

• customer satisfaction.

8.3.3 Input to design and development

The activities of verifying the design and development aim to control that the products of each phase have been realized as planned and that the results obtained meet the initial requirements.

Verification activities are constantly performed by the Delivery Supervisor and summarized in the periodic work progress statuses.

The inputs always include:

· evaluation if and how the activity falls within the perimeter of Core activities

· economic evaluation of resources and personnel to be employed

· technical evaluations

· strategic evaluations on IP used/created,

flows are also managed by the online tool GitHub.

8.3.4 Controls of design and development

The design review aims to comprehensively and critically analyze the results of the functional specification and technical drawing phases, in order to verify that the products meet the initial requirements and can proceed to the realization phase.

The realization review aims to verify that the software modules implement the requirements, reflecting the functional and technical specifications.

The depth of the reviews is commensurate with the complexity and risks of the project, service, or product.

The criteria that allow moving on to a subsequent phase are defined in the project plan (or of the service or the product). The responsibility for conducting the reviews and moving on to the next phase is of the Project Owner, who keeps the minutes in the project archive. If deliveries to the Customer are planned, the transition to a subsequent phase is determined by the acceptance of the functionalities expected in the sprint.

Reviews, also organized in the form of kick-off meetings, are attended by representatives of all corporate functions interested in the subject of the review and, if necessary, other expert personnel.

The review activity is recorded in a special meeting report.

Design validation

In the development of the documents, control points (work plan milestones) are provided for the verification of the validity of the project, service, or product, and to undertake appropriate corrective actions in relation to the progress of the activities.

In particular, suitable methods for handling operational problems, non-conformities, and requests for modification by the Customer are provided.

After agilelab's internal test, the final validation of the software is carried out through the test by the Customer leading to the acceptance of the project, or of the deliveries planned for services or products.

Critical projects, services, and products are subjected to Risk Management activity, according to suitable criteria for identification, classification, analysis, monitoring, and mitigation (see Gitlab - Repository)."

Additional INFO on the Compliance Sharepoint.

.3.5 Output of Design and Development

The outputs of the design process are all the documents that, with increasing levels of detail and refinement, define all the specifications necessary to meet the input requirements.

8.3.6 Design and Development Changes

agilelab manages not only new projects but also modifications to previously validated projects.

In the case of a modification to an existing project rather than a new project, the BU Manager, together with the team, will make the necessary changes and update the product revision index.

All changes are discussed, reviewed, approved, and verified using similar methods as those used for the initial release. Changes that require an update to the project proposal and replacement of the resulting document are adequately documented and communicated to all stakeholders.

All changes requested during the design and development phases are integral parts of the Change Management and Configuration Management processes and are stored in the Gitlab repository.

8.4 Control of externally provided processes, products, and services.

8.4.1 General

agilelab has defined specific methods for selecting, qualifying, evaluating, and monitoring suppliers, as well as managing procurements that influence the quality of the provided services, including the supply of physical goods, equipment, services, and the acquisition of professional collaborators such as instructors and consultants.

All interactions with potential suppliers, including the definition of contractual conditions (and any modifications thereof), issuing orders, and handling disputes, should be managed by the purchasing function.

The main aspects related to the procurement of physical goods that the organization considers are:

Identification of product and service specifications.
Evaluation of product/service costs in relation to performance characteristics and delivery times.
Administrative aspects in supplier/partner relationships.
Logistic requirements.
Documentation related to procured products.
Aspects related to the delivery of procured products.
For the procurement of equipment and services, "Purchase Orders" or specific orders are used.

The company departments may request quotes/offers from suppliers or gather information over the phone regarding the description of the requested supply.

The orders should be detailed and complete, including both commercial aspects and the technical and qualitative requirements requested by the respective departments, and should contain all necessary references or attachments of technical specifications or other identifying criteria needed to clearly define the purchase.

Alternatively, if the offers from suppliers are considered satisfactory and complete for agilelab, they may be accepted; otherwise, they need to be supplemented.

The supplier is required to confirm acceptance, either by responding or by tacit acceptance.

In the case of modifications to existing orders, the new technical specifications and any accompanying documents, if modified from the pre-modification version, should be sent to the suppliers. Any changes are clearly mentioned on the purchase orders, and the modified document/specification is always specified, along with the identification of the pre-modification purchase documentation. The purchasing function is responsible for monitoring the progress of deliveries related to purchase orders.

Outsourcing may occasionally involve certain processes (such as training and occupational safety consulting) that are outsourced to external suppliers. In these cases, orders are formalized as standard supplies, but specific quality assurance requirements are also indicated.

General economic and qualitative conditions to be applied to subsequent supplies are defined.

8.4.2 Type and extent of control

Considering that agilelab's production processes do not rely on material procurement, the verification carried out by agilelab for purchases involving physical delivery of goods focuses on ensuring the good condition of the purchased items and the correspondence between what was ordered, acquired, and documented in the accompanying documentation. The responsibility for these checks lies with the department that requested the procurement.

In case of a negative outcome, the product is identified to prevent unintended use, and the process for managing non-conformities, as described in Chapter 10 of this guideline, is initiated.

When specified in the contract and/or regulations, the client/contractor has the right to verify, at agilelab, that the purchased items comply with the specified requirements. The same right is granted to the entire client system under the same conditions.

Outsourcing

agilelab utilizes external suppliers to support its production process (consultants, partners). To regulate these relationships and ensure control, agilelab has established methods and procedures for controlling these suppliers, including:

Verification of the competencies and specific qualifications of each external collaborator/partner.
Supplier qualification and monitoring activities.
Acceptance controls of services.
Audits at the suppliers' premises.
Depending on the criticality of the supply and the supplier, RSQ evaluates the use of one or more of the aforementioned tools.

8.4.3 Information to External Suppliers

For supplier management, a specific Operating Instruction IO 04 Supplier Management has been created.

8.5 Production and Provision of Services

8.5.1 Control of Production and Service Provision

This section applies to the production and delivery processes referred to in Chapter 1 "Purpose and Scope of Application" of this guideline.

General

For the proper management of business processes, it is necessary for these processes to take place under controlled conditions.

The company pays particular attention to process variables that can negatively influence the outcome, taking into account:

Technologies used.
Server systems used.
Technical skills.
Methods.
Regarding "technical skills," agilelab places particular emphasis on the training and development of its personnel, as defined in Chapter 7 of this guideline.

"Methods" of work are formalized, when necessary, through specific operating instructions (IOs) made available to the staff.

The "technologies" component is continuously evolving and carries the risk of not offering an up-to-date service to the client. agilelab constantly monitors technological advancements, striving to use the most effective and efficient ones that are also compatible with the acquired or acquirable training within the project's context.

Production and Service Programming

Service provision is carried out for new products or modifications to existing ones. PRG, if necessary in collaboration with RQ, defines all the activities, resources, responsibilities, and timelines required to develop the product process according to the specific requirements of the client, the internal organization, the internal schedule, and workloads.

This operational activity is performed with considerations and the following activities:

Verification or definition and issuance of documents by the design team.
Verification or definition and issuance of control documents/reports by RQ.
Definition of equipment for the service.
Definition of potential suppliers and procurement documents by ACQ.
Definition of specific Operating Instructions (identification).
Evaluation of training and specific courses.
agilelab defines controls throughout the processing stages, from acceptance control to final control, using various types of documents.

Upon customer request (assisted by CQ and potentially PRG), agilelab prepares or updates the Control Plan (GITLAB).

The service provision programming is carried out by PRG based on data collected from customer orders following the "contract review" activity performed by COM.

PRG manages the programming based on the acquired orders or agreements made directly, by phone, or via email.

The preliminary programming is performed by inputting received orders into a "Customer Order Table," which consolidates all the relevant data for identifying the client, the service to be provided, and the delivery dates to be managed. It can be updated daily and, when necessary, distributed in electronic or hard-copy format on the company notice board in the Quality office.

During periods of high workload, PRG may provide indications regarding the products to be delivered based on the dates of the following days/week to facilitate work organization within the company.

PRG promotes the completion of processing through CP, which monitors the progress of the operations, supported by CQ, who prepares the necessary documentation and control materials to be delivered to the Service Providers/ADD.

AMM manages document issuance for delivery to customers, handles accounting activities, registers sales, and handles invoicing.

CTP, in addition to extracting accounting data for fiscal and contributory requirements, summarizes economic and financial data for analysis by the Management, concerning the defined monitoring and measurement indicators.

PLANNING

Controls are carried out as needed through self-inspection or upon specific customer requests by the Service Providers/ADD, using "Control Plans" for recording purposes.

In the case of processes outsourced to suppliers, they are required to provide guarantees of control records, and if necessary, relevant documentation (their registration reports).

The frequency of controls and recording is managed by IT according to the number of customer requests and is indicated on the planning documents.

The Service Providers/ADD must adhere to the instructions provided for processing and control activities, and in case of process-related issues, they must follow the procedures described in Chapter 10 of this guideline.

Any additional controls carried out by IT sporadically as samples or for complex operations, if performed, should be recorded by IT on the existing forms used by the service providers or attached to them.

No product/service can be provided without undergoing the established controls or receiving authorization recorded by IT.

In the event of non-conformities, after reworking the products/services, they must be rechecked, and records of these checks must be maintained.

REALIZATION

The final control defines the method and responsibility for product/service release, including verification that:

All controls established through the "Control Plan/Control Plan" or "Processing Cycle" have been carried out as planned and recorded.
The products/services do not have any defects.
The products are delivered correctly (according to their own specifications or customer specifications, if applicable).

8.5.2 Identification and Traceability

agilelab has defined specific methods to ensure the identification and traceability of documents related to each project and order throughout the entire activity management cycle.

Document traceability refers to the system that allows reconstructing the history of service delivery, from the issuance of the offer or participation in the tender to the registration of activities performed, related controls, and obtained results.

8.5.3 Customer or External Provider Property

To date, the company is not in a position to manage customer properties such as materials and equipment provided for use during consulting activities carried out at the customer's premises.

In case agilelab uses goods and products provided by the customer for its activities, the following activities are foreseen:

Verification of adequacy and functionality of equipment/infrastructure according to contractual agreements.
Definition of management and usage procedures.
Notification to the customer in case of inadequate, lost, or unsuitable products through written communication by the Project Manager.
If customer properties are damaged, it is the responsibility of RQ to inform the respective customer and open an internal non-conformity according to the procedures described in Chapter 10 of this guideline.

Regarding the collection, processing, and protection of provided data, reference is made to the provisions stated in the Instructions for Information and Personal Data Security.

agilelab is committed to confidentiality and respecting intellectual property concerning data/services of clients that it becomes aware of, and ensures compliance with any applicable clauses regarding the use of client-owned products/services.

8.5.5 Post-Delivery Activities

After the sale of products and services to clients, agilelab is available to address any doubts, suggestions, or complaints from them. The organization has communication channels in place that allow all clients to immediately contact the relevant company representatives and discuss relevant topics.

Complaints regarding the quality of the product or service are managed by SQ in consultation with the respective Area Director, in accordance with the provisions outlined in Chapter 10 of this guideline. After handling the complaint, RSQ will assess the actions to be taken, including any corrective actions, following the procedures outlined in Chapter 10.

The products and services delivered do not generate waste that needs to be managed; therefore, it is not necessary to define specific disposal methods.

8.5.6 Control of Changes

The management continuously monitors the company's performance, and if deemed necessary based on appropriate evaluations, decides to make changes to the process. The management is responsible for defining the individuals responsible for implementing these changes and ensuring that they are implemented as specified. Furthermore, the management verifies the congruence of the expected results following the implemented changes.

If issues arise during the service delivery process, it is the responsibility of the Function Manager to inform RQ, who will initiate an internal non-conformity as described in Section 8.7 of this chapter.

8.6 Release of Products and Services

To ensure that products are delivered to the customer only after being checked, agilelab has empowered everyone to perform the required checks based on the category of the product and service in question.

After completing the final phase of the activity, the release is carried out through the specified controls.

The Business Unit Manager or designated personnel conduct the necessary checks before delivering the product to the customer to verify conformity prior to release.

In case of non-conformities during the deliberation phase, it is the responsibility of RSQ to open the corresponding non-conformity and proceed as described in Section 8.7 of this chapter to resolve the identified product anomalies.

8.7 Control of Non-Conforming Outputs

agilelab carries out checks on products during acceptance as well as during the development, design, service provision, and prior to customer delivery phases.

The externally entrusted process parts are verified before customer delivery, as indicated in this chapter.

Regardless of the work phase in which a non-conformity is identified in the product/service, it is reported by the individual who detects it to the responsible person in the respective area, who informs RQ to open the non-conformity.

Non-conforming products and services, by the person who detected the anomaly, are systematically blocked to prevent unintentional continuation of the activity or delivery to the customer before resolving the identified issue.

Upon the occurrence of a non-conformity, RQ should be informed by the function that detected it or their respective manager to promptly intervene and take the necessary actions to resolve the non-conformity and prevent its recurrence.

RQ is responsible for defining the treatments and providing instructions in this regard.

RQ records the non-conformity and the actions taken.

RQ is responsible for verifying the implementation of the prescribed actions and their effectiveness.

If the non-conformity is attributable to the supplier, RQ will record it regardless of the phase in which it is detected. Furthermore, RQ will communicate the issue to the supplier by providing:

Information about the identified non-conformity.
Decisions made regarding the non-conformity.
Request for appropriate corrective actions.
RQ verifies the implementation of the prescribed treatments.

Any non-conforming product undergoing rework must be rechecked according to the specified controls once it has been rectified.

RQ, with the collaboration of various function managers, is responsible for studying the causes and opening Corrective Actions if necessary.

RQ periodically analyzes the information related to non-conformities using specific statistical techniques.

RQ examines non-conformities, evaluates the causes, and assesses the likelihood of recurrence to define appropriate corrective actions aimed at preventing their recurrence over time.

It is the responsibility of RQ to inform the Management so that this information can be used as input for updating risk and opportunity analysis, if deemed necessary.

8.8 Business Continuity

8.8.1 Operational Planning and Control

agilelab plans, implements, and controls the necessary processes to meet the requirements of Business Continuity and takes actions to address risks.

Additionally, the organization implements plans to achieve Business Continuity objectives.

The company manages and monitors the implemented actions, evaluates their effectiveness, and manages non-conformities and corrective actions.

The reference instruction is IO 8.8 Business Continuity Management.

8.8.2 Business Impact Analysis and Risk Assessment

The Business Impact Analysis (BIA) is the assessment of the impact on the business in the event of significant incidents that could jeopardize business activities and service delivery. The BIA identifies service restart requirements to guide appropriate Business Continuity solutions.

agilelab performs the Business Impact Analysis with the aim of:

Identifying relevant business services.
Assessing the impacts related to service unavailability.
Identifying contractual/regulatory recovery times.
Identifying the most critical services.
Identifying activities that need to be resumed at a later stage.
The agilelab Business Impact Analysis mainly includes the analysis of business services and identification of critical activities using evaluation parameters that consider regulatory constraints, contractual constraints with clients, relevance of the service/activity to the business, and strategic significance to the company.

The BIA is updated with information provided by the involved company departments and made available for the preparation of Disaster Recovery plans.

Risk assessment is aimed at preventing and mitigating threats that could impact company assets, including their availability for an extended period. Risk assessment helps protect the company's value, achieve business objectives, and manage risks that could result in the loss of confidentiality, integrity, availability, and compliance of business services/processes.

The decision on how to treat identified risks is based on evaluating the right balance between compliance with mandatory regulations, business needs, cost, technology, and security. The treatment options for identified risks include mitigation, acceptance, avoidance, or transfer.

Risk analysis is conducted for agilelab's initiatives (projects or orders) and business services.

The company has determined the frequency of risk assessment related to Business Continuity:

Annually as part of the management review, in the absence of non-conformities.
In the event of a non-conformity.
With significant changes to company assets.
With significant changes to the context (internal/external).
8.8.3 Business Continuity Strategy

The goal of the Business Continuity system is to ensure that the organization develops plans, instructions, and processes and provides suitable facilities and equipment to promptly manage incidents and crises.

The development and implementation of a Business Continuity response are derived from creating a management model and an event management structure, along with Business Continuity and Disaster Recovery plans that detail the steps to be taken during and after an incident to preserve or restore operations.

The system enables:

Confirming the nature and extent of the incident.
Initiating an appropriate Business Continuity response.
Having plans, processes, and procedures for activation, coordination, communication, management, and closure of the emergency.
Having the necessary resources to support plans, processes, and procedures in managing the incident.
Communicating with stakeholders, i.e., the key parties involved.
To be prepared to handle emergency situations and crises effectively, agilelab has developed the following types of solutions:

Organizational solutions.
Logistical solutions.
Technological solutions.

8.8.4 Plans and Procedures for Business Continuity

agilelab has developed an Emergency and Crisis Management Process to describe the activation methods, responsibilities, and necessary contacts to manage an emergency and/or crisis situation in the company.

The Emergency and Crisis Management Process consists of three phases: Incident, Emergency, and Crisis.

agilelab distinguishes events based on their impacts on the company (ICT, NO ICT, Cyber), thereby identifying the incident management procedures to follow and the teams to involve.

The activation of the Emergency and Crisis Management Process becomes necessary when an event presents risks and impacts of such severity that they cannot be managed through the existing incident management procedures in the company.

The actions to be taken and the corresponding execution responsibilities in the event of an emergency requiring the evacuation of the building or specific areas to ensure the safety of individuals are described in specific documents: IO 10.1 Business Continuity Plan.

Specifically, the Business Continuity Plans (BCPs) describe:

Hypothetical disaster scenarios.
Contact and communication management during emergencies or crises (internal resources, suppliers, customers, partners).
Proposed containment and/or mitigation actions for each scenario.
Reference documentation for each service.
The Business Continuity Plans also include the contact information of the operational staff to be involved.

Disaster Recovery Plans (DRPs) are a set of documents prepared for the restoration of technical and application infrastructures, outlining the methods and timing for reactivation at the Disaster Recovery site.

Service delivery in the Disaster Recovery environment is ensured by dedicated hardware, software, connectivity solutions, and the availability of adequately trained internal personnel.

Prevention, monitoring, and control systems have been implemented to address events related to information and logistical terrorism.

To improve the efficiency and operational continuity of the services provided, agilelab has evolved from a primary and secondary site-based approach for many infrastructures to a parity site-based approach. This means that some services are simultaneously delivered from both sites, while others are provided from one site but can be activated on the other site when needed. The local network, geographically distributed network, and access to the telephone network have been designed and implemented to ensure the same reliability and scalability across all locations.

The complete implementation of Business Continuity relies on developing a direct collaboration relationship with customers, which enables the analysis, definition, and implementation of joint measures for crisis management and service recovery, including lines, security devices, and connections to International Circuits.

agilelab agrees with its customers/suppliers on collaboration modalities, such as defining the Recovery Time Objectives (RTO) and Recovery Point Objectives (RPO), connections to be used in the event of a disaster, contact references for general communications, actions to be taken when activating the technical and application infrastructure from the Disaster Recovery site, and planning for tests.

8.5 Exercise Program

agilelab regularly conducts Business Continuity and Disaster Recovery tests and exercises, which can be of various types:

Organizational (e.g., Emergency Management Tests, Business Continuity Plan tests, etc.)
Logistical (building evacuation drills)
Technological (Disaster Recovery tests)
Business Continuity tests are conducted to assess the effectiveness and efficiency of the Emergency Management Process, verify the adequacy, completeness, and functionality of Business Continuity Plans and supporting procedures, evaluate the readiness of Business Continuity management teams and the effectiveness of the awareness program, and develop and promote knowledge and awareness of Business Continuity issues within the company.

Disaster Recovery tests are necessary to assess the functionality and efficiency of technological infrastructures and keep the skills of involved personnel up to date. Annually, agilelab prepares a multi-year plan for Business Continuity tests and Disaster Recovery tests.

# 9 Monitoring, Measurement, Analysis, and Evaluation

9.1.1 General

The organization has planned and implemented, as defined in this document, monitoring, measurement, and improvement processes to ensure product conformity to requirements, ensure the security of managed information, ensure compliance with the integrated management system, and ensure its continuous improvement, including the use of statistical techniques. Additionally, it assesses the performance of business continuity and its effectiveness.

9.1.2 Customer Satisfaction

The organization has defined a method for monitoring customer perceptions of how well it meets their requirements.

To this end, the sales functions conduct telephone interviews with their customers regarding their satisfaction with the products offered and the organization in general.

The level of customer satisfaction is then analyzed by RQ, in collaboration with the sales function and DG, using data derived from "mystery shopper" interviews, taking into account the register where customer feedback is recorded, and considering the information provided by sales representatives. This is done on a sample of customers representing at least 50% of the revenue. Telephone information is collected to assess the perceived satisfaction value and importance assigned by the customer for defined attributes, evaluating the customer's satisfaction with the product offered and the organization in general.

RQ calculates the average satisfaction and importance values expressed and presents them in a specific chart.

Responses are quantified in terms of satisfaction/importance for all product/service attributes defined. If a value lower than 3 is identified, the motivation is analyzed and reported in the table containing the conclusions.

After the necessary evaluations, the organization assesses any corrective actions or the definition of new indicators to be included in the company's indicator list.

Upon completion of the activity, which is conducted at least once a year, RQ, together with DG, compiles and formalizes the results on a specific Excel form "Customer Satisfaction Questionnaire." The same document also includes the sampling method used for selecting customers to be interviewed and a comment.

The measurements taken and their deviations are an input for the Management Review process. Within this activity, or for specific needs or critical situations that arise during the year, the results are evaluated and serve as input for defining improvement plans if necessary.

Furthermore, customer feedback, although not considered as complaints, serves as input for modifying existing products.

9.1.3 Analysis and Evaluation

agilelab considers it essential to define specific "indices" and employ appropriate statistical methods to measure (monitor) numerically the effectiveness of the quality system and information security in accordance with the company's policy.

The Management reviews the data to assess the performance of the Integrated Management System, as well as its individual elements, processes, and products, in order to proactively address potential areas at risk and define any necessary improvement actions that emerge from the analysis.

The goal of the team is continuous improvement, which is achieved through effective corrective actions taken based on the analysis of historical data.

The General Management, in collaboration with RSQ, reserves the right to modify the applied statistical indices over time and may supplement the data with graphs or tables.

These indices include:

Measurement of business processes
Information risk analysis
Product and service conformity
Planning effectiveness
Measurement of customer satisfaction
Business objectives trends
Incoming inspection trends
External supplier monitoring indices
Management system effectiveness
Activities planning, such as audits, backup restoration verification, continuity tests
Detection of events on computer systems
System and component downtime
Incident resolution times
Number of customer complaints related to information security
Impact of penalties related to information security on revenue from business continuity
External intrusions/attacks
Non-conformities and complaint trends
Root causes of non-conformities
For the following indices, monitoring and measurements must be carried out continuously with monthly reports:

System and component downtime
Incident resolution times
Number of defects (bugs) detected in developed software
Number of customer complaints related to information security and business continuity
Impact of penalties related to information security and business continuity on revenue
Number of external intrusions/attacks
The data obtained from the indicators and statistics are discussed during the annual Management Review and serve as a fundamental element for defining any corrective actions or improvement plans. They play a crucial role in agilelab's pursuit of continuous improvement.

9.1.4 Evaluation of Business Continuity Procedures

agilelab measures the performance of Business Continuity and Disaster Recovery tests using the Severity Code and reviews the Business Continuity procedure.

Additionally, the Business Continuity procedure is reviewed during post-incident evaluations and in the case of significant changes.

During the Management Review, an evaluation of the Business Continuity procedure is conducted.

9.2 Internal Audit

All elements of the Integrated Management System undergo internal audits and are regularly evaluated based on the status and importance of the activities to be verified.

Internal audits are adopted by the organization, particularly to verify if the activities relevant to quality meet the reference standard, the conditions specified in the reference documents, and if what is defined and implemented is effective and in line with the established objectives.

The Management utilizes internal audits as an assessment tool for the Integrated Management System and to identify areas within the company where performance can be improved. The effectiveness and efficiency are evaluated based on objective evidence.

The internal audits aim to objectively evaluate the following areas or activities:

Organizational structures
Compliance with the reference standard
Availability of resources, such as personnel and equipment
Work areas
Documentation, reports, and their archiving
The main aspects considered during internal audits are:

Efficiency and effectiveness in implementing processes
Reverification of identified risks
Opportunities for improvement
Use of statistical techniques
Results and expectations regarding process and product performance
Relationships with stakeholders
Audit Planning

RSQ plans internal audits to cover all areas of the quality system proportionally to the importance of the examined sector.

The annual program (Mod 09.01) is prepared within the first quarter of the audited year and includes:

Names of personnel responsible for internal audits
Functions/areas to be verified and the scope of the audit
Name of the manager of the area to be verified
Planned date/actual date
It is the responsibility of RQ to coordinate dates, times, verified functions, and seek approval from the CEO for the planned schedule.

The annual program may be updated based on needs such as:

Requirements for further knowledge gained during surveillance activities
Acquisition and implementation of new processes that significantly modify operational instructions
Internal audits are conducted by qualified internal staff or external consultants (with independence of judgment from the area being evaluated).

Requirements for internal auditor qualification:

Participation in ISO 9001 and UNI E ISO 27001 and ISO 22301 quality system evaluator courses at an accredited certification body
Familiarity with relevant sector legislation
Shadowing a qualified auditor in at least 2 internal audits across the entire system
Any personnel lacking independence of judgment for the sector under examination will be excluded from the audit team.

Audit Execution

At least two weeks before the scheduled verification, RQ sends an internal communication to the responsible persons in the audited areas, indicating the date, time, and purpose of the audit.

Before starting the audit, the Audit Manager gathers:

Copies of system documentation (procedures/instructions) related to the areas to be verified
Evidence Collection Forms or checklists
Reports resulting from previous audits of the same area
The Audit Manager conducts the activity with the collaboration of the responsible person from the area being examined, visiting the area and verifying the existence of applicable documentation (instructions, specifications, etc.) and the use of related supports (forms, modules, etc.), as well as the presence of quality records, evaluating their effectiveness. The evidence collected is noted using Evidence Collection Forms or checklists.

Audit Report Preparation and Anomaly Management

All gathered information is processed and documented by the Audit Manager through an Internal Audit Report, which is then distributed to the audited function for review. The function must sign to acknowledge receipt of the report, which is archived by RQ.

The Audit Manager and the audited function analyze the findings, highlighting:

Organizational deficiencies, operational inadequacies, lack of resources, potential non-conformities
Priority of interventions based on the seriousness of identified shortcomings
Opportunities for improvement
From this activity, the definition of appropriate corrective actions or improvement plans emerges to eliminate the identified non-conformities, prevent their recurrence, or enhance effectiveness.

During this phase, RQ plans verification dates for the execution/effectiveness of the actions taken. The audited function is responsible for implementing the defined actions within the specified timeframe and reporting completion dates and any encountered difficulties to RQ.

If, during the audit, the action has not produced the desired effects, RQ, in consultation with the audited function, proceeds to redefine any additional actions, which will be managed following the above-described process. The corrective action and improvement plan are considered concluded when their effectiveness has been positively verified.

The Quality Manager is responsible, during the Management Review, and whenever deemed necessary or appropriate, for providing the results of the audits to the CEO for final considerations regarding the outcome of internal audits.

This report aims to provide objective information to the General Management to evaluate the effectiveness of the Integrated Management System during the review.

9.3 Management Review

9.3.1 General

Periodically, the Quality Manager informs the General Management about the performance of previously defined statistical indices, allowing the Management to conduct an initial review.

It is the responsibility of the General Management, after analyzing the data provided by the Quality Manager, to hold a meeting with agilelab employees to present and discuss the performance in relation to the company's policy.

To ensure that the Management can monitor the effectiveness of the data analysis, RQ notifies the General Management of the performance of key statistical indices, progress of actions and improvement plans, as well as the analysis of supplier ratings by January of each year. The meeting is documented on a generic form, referencing any decisions made.

By July of each year, the General Management analyzes the above elements and the results of the mid-year meeting.

After the analysis, the General Management formalizes its considerations through a written report (using a generic template) summarizing the overall assessment of the effectiveness of the established management system.

The Management reserves the right to conduct extraordinary reviews in the event of any identified anomalies or changes or innovations in the company's processes.

9.3.2 Inputs to the Management Review

To conduct the review of the Integrated Management System, the following factors have been identified as essential elements:

Results of Internal Audits and second- and third-party audits
Market analysis
Risk analysis
Analysis of the evaluation of system risks and information security
Analysis of the controls implemented to mitigate risks to information security
Training needs
Need for new resources (personnel, equipment, machinery)
Resource requests from Function Managers
State of the work environment and infrastructure
Measurement indicators of business processes
Customer feedback
Previous reviews
Measurement indicators for service conformity related to customers
Indicators for measuring customer satisfaction
State of corrective and preventive actions
Supplier issues
Issues related to outsourced processes
Relevant communications from external stakeholders
Complaint trends
Business continuity plan
Status of investigations into system non-conformities and corrective actions
Achievement of quantitative objectives related to the Policy established by the General Management during the review
Results of internal audits of functions
Analytical summaries
Status of ongoing improvement plans
9.3.3 Outputs of the Management Review

Upon completion of the review, the General Management defines the necessary corrective actions, resource requirements, establishes new objectives to verify the continuous improvement of the organization's functioning, sets parameters for the following year, issues a document on the Integrated System Review using a generic template, and schedules a new meeting with the employees to explain the outcomes.

A copy of this review will be distributed to the Quality Manager, who will create a Business Continuity review report to be published on the website for sharing with clients.

The General Management also determines the actions to be taken and any modifications to be made to the management system based on the Management Review.

Within the Management Review of the Integrated Management System, the General Management also addresses security-related issues, specifically:

Where possible, the results of internal audits on the facilities
Security policy and objectives

# 10 Improvement
10.1 General

agilelab utilizes the data it possesses and its assessments to define appropriate improvement actions aimed at maintaining and increasing customer satisfaction.

The company considers improvement actions to include corrective actions, improvement plans, operational changes, and innovations.

The organization has established a system for continuous improvement by analyzing:

Achievement of improvement plans
Risk reduction
Achievement of defined objectives
Adequacy of the Policy
Customer satisfaction analysis
Corrective and preventive actions
Audit results
Based on this analysis, new objectives are defined to pursue the continuous improvement of the system itself.

10.2 Non-conformities and Corrective Actions

Corrective Actions

The top management of agilelab ensures a constant commitment of resources in identifying and managing tools for improvement.

RQ is responsible for coordinating the activities necessary for eliminating the causes of non-conformities and promoting and incentivizing improvement activities.

The need to activate the aforementioned actions occurs in reference to defined types of events, as identified below.

Corrective Actions

In agilelab, the implementation of corrective actions begins with the identification of a non-conformity or a tendency towards a condition contrary to the quality of the product/service or the system. It involves taking measures to eliminate, minimize, or prevent the recurrence of the problem.

The company pays particular attention to potential "errors" or those committed by its own organization in order to learn and increase its know-how. Based on the company's policy, it seeks continuous improvement of the product and service offered to customers.

Furthermore, corrective actions can be implemented following:

Reports from third parties
External attacks on systems
Incidents impacting information systems
Bugs detected in production software
Operational incidents
Tests of the operational continuity plan
The Quality Manager is primarily responsible for coordinating the activities required for the elimination of the actual or potential causes of non-conformities. Identifying the cause or potential causes may involve modifying processes and/or revising the management system itself.

Permanent changes resulting from corrective actions are consequently documented in the integrated management system documentation.

The Quality Manager is also primarily responsible for verifying the execution and effectiveness of the actions.

The Quality Manager is responsible for managing the actions and their documentation.

The respective Function Managers are responsible for proposing actions related to their areas.

The activities for implementing corrective or improvement actions go through the following phases:

Identification of the need for intervention
Planning of the intervention
Implementation
Verification of execution and effectiveness
The need to activate the aforementioned actions occurs in reference to the following events:

Corrective Actions

Non-conformities
Internal audits (process/system)
Customer complaints
Integrated Management System Review, identified internally or by external entities
Statistical analysis
RQ, in agreement with the various Function Managers, records the plan of activities necessary for resolving the reported issues on a specific form.

This plan includes:

Clear identification of the underlying causes/motives for the intervention
The objectives to be achieved
The sequence and activities to be performed
The resources needed to achieve the objectives
The names of the individuals responsible for carrying out the activities
The deadlines by which the activities must be completed, executed, and evaluated for effectiveness
RQ is responsible for coordinating the execution of the activities, ensuring compliance with the timelines, and managing/recording any changes. RQ is also responsible for updating the status of the actions taken.

If there are discrepancies between what was planned and what was executed, RQ, in collaboration with the audited function, updates the form.

RQ or the responsible function verifies and records (for closure) that what was planned and agreed upon is executed within the specified timeframes and that the predefined objectives are achieved, or that the actions taken have been effective.

If the action has not completely or partially eliminated the actual or potential causes of non-conformities or has not produced the expected results, FA, in collaboration with RQ, takes further studies and potentially additional necessary actions.

Customer Complaints

Complaints are forwarded by the customer via email, phone, or direct reporting to the sales area functions. These complaints can pertain to non-conformity with product requirements or non-conforming service delivery.

Complaints can be received regarding:

Products
Services
Information security
Administrative anomalies/other
When NCs arise from reports or complaints received from customers, the following possibilities arise:

The complaint relates to processing/product
The complaint relates to the service (delivery times, missing documentation)
Complaints are always communicated to RQ for management. RQ may collaborate with CQ, CO/PRG, or other relevant functions in handling the complaint.

If the complaints are received in the form of observations and if requested by the customer, customer-specific forms can be used, if they have a field for management. Otherwise, the information can be transcribed onto internal forms.

If the complaints are received from the customer verbally and pertain to processing, the internal form must be completed and managed. If the complaint is related to the service and is of minor significance (where only clarification is required and no corrective action is necessary), the same internal form mentioned above can be used, or at least a written response to the customer can be prepared, such as a "Response to Complaint (fax or email)."

RQ examines the issue, communicates it to the relevant Company Functions (FA), specifies the treatment performed, and assesses the need for any corrective actions, as well as communication with the customer.

Complaints may lead to corrective actions if deemed necessary, managed in the same manner as internal NCs and using the customer-specific form if applicable.

RQ keeps a summary of the received and managed complaints for reporting and data analysis purposes using an index generated directly by the computer.

All documentation related to complaints is maintained by RQ.

Process/System Non-Conformities

System non-conformities can be identified during internal audits, external audits, or in-process by RFU or operators.

Each company function is required to report to RQ any misalignment identified in the process or system. Upon receiving any such reports, RQ opens a corrective action, as defined above, using the form, to eliminate the actual and/or potential cause.

Corrective and improvement actions are handled as defined above.

IT Incident Management

Any violation of the incident management policy must be reported as soon as possible through the reporting procedure.

Security incident violations can be reported in various ways. For management purposes, an Operating Instruction IO 12 - IT Incident Management Reporting and Management has been defined.

10.3 Continuous Improvement

The top management of agilelab ensures a constant commitment of resources in identifying and managing tools for improvement.

RQ is responsible for coordinating the necessary activities to incentivize improvement activities.

The management, to determine the need or possibility of implementing improvement actions, considers the following as inputs:

Validation data
Process performance data
Test data and self-assessment
Established requirements and feedback from interested parties
Analysis and evaluation results
Review results
Resource or investment budget needs
Strategic/management decisions of the management
Based on this analysis, new objectives are defined to pursue the continuous improvement of the system itself.

RQ prepares the improvement plan on a specific form, indicating a plan of activities that the organization deems necessary to pursue continuous improvement.

This plan includes:

The objectives to be achieved
The sequence and activities to be performed
The resources needed to achieve the objectives
The names of the individuals responsible for carrying out the activities
The deadlines by which the activities must be completed
The Quality Manager is responsible for executing or ensuring the execution of the planned activities within the defined timelines.

The Quality Manager is also responsible for updating the progress status of the improvement plan.

If there are discrepancies between what was planned and what was executed, the Quality Manager updates the form.

The Quality Manager also plans the necessary verifications to ensure the effectiveness of the improvement plan directly on the form.

The Quality Manager, together with the management, verifies that what was planned and agreed upon is executed within the specified timeframes and that the predefined objectives are achieved, or that the actions taken have produced the desired effectiveness.

Upon completion of the improvement actions, RQ signs the document, approving it.

























