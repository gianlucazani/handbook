# Milano

- Address: [Bastioni di Porta Nuova, 21, 20121 Milano MI](https://goo.gl/maps/rpuWzVjZV8LKjy8c9)
  + [Spaces Porta Nuova](https://www.spacesworks.com/it/milan-it/porta-nuova/)
- Parking:
  + Inside parking not available right now.
  + [Underground Car Parking XXV Aprile](https://www.apcoa.it/en/parking-in/milano/xxv-aprile/).
- Public transportation in Milan is handled by [ATM](https://www.atm.it/en/Pages/default.aspx):
  + By metro: 
    * *Garibaldi* stop, then five minutes by walking (best option if you're coming from the [*Milano Porta Garibaldi*](https://www.trenord.it/en/routes-and-timetables/journey/station/?no_cache=1&mir-code=S01645&cHash=f02d02f98fa66fbe464b7171d7c5f1b5) train station).
    * *Moscova* stop, then five minutes by walking.
  + It is possible to purchase ATM tickets for the metro downloading the [*ATM Milano Official App*](https://www.atm.it/en/ViaggiaConNoi/Pages/ATMMobile.aspx).
- Seats:
  + 18.
  + The seats are not named.
  + A *badge* is required to enter.
  + The badges can be retired at the reception (Monday - Friday, 8:30 a.m. - 7 p.m.)
- Office spaces: 
  + _Operative area_:
    * Third floor:
      - Agile Lab offices (310 + 311-312):
        1. One small office with 3 seats.
        2. One large office with 6 seats.
    * Ground floor:
      - Coworking open-space with 9 seats for Agile Lab.
      - Phone boots, where you can make calls (no booking and no time limit).
    * Underground floor:
      - Meeting rooms.
  + _Recreative area_:
    * Ground floor:
      - Coffee/snack bar.
      - Relax area.
    * Fifth floor:
      - Panoramic terraces accessible for smoking, taking a break, eating...
