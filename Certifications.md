# How to get certifications

As part of the benefits offered to employees, Agile Lab offers the opportunity to get professional tech certifications, covering costs and providing support along the preparation phase.

The {% role %}Training/Certification Coach{% endrole %} will help you to define feasible goals and to prepare for the exam.

At the beginning of the year, the CTO will set:
- a goal regarding the number and type of **new** certifications that should be obtained during the year by the whole company
- a goal regarding the minimum number and type of certifications that the company as a whole must **maintain** among the existing ones.
These goals might be modified during the year to meet specific needs as opportunities arise.

The **new** certifications are considered "**priority 1**". You can find the certification basket inside the "Big Data" SharePoint at the path `Certifications/Certification Plan 202x`. People that will contribute to meet this achievement will be also rewarded, but only if the overall goal will be reached, like in a real team play.

To encourage candidates to take exam earlier, priority 1 certifications achieved in the first quarter of the year (until March 31st) will guarantee a base reward regardless of the overall reaching of the goal.
Moreover, the possible base reward will be higher for people getting certified earlier.

| Quarter | Prize multiplier |When |
|--------|------------------|---|
| Q1     | x1.5             |ALWAYS, at the end of the year |
| Q2     | x1.25            |the overall certification plan goal will be reached|
| Q3     | x1.25            |the overall certification plan goal will be reached|
| Q4     | x1               |the overall certification plan goal will be reached|
There is nothing inherently bad to try it later in the year, but the risk is that there is not enough time to retry a second time or that the Certification Coach can’t find some other candidate if somebody backs out too late.

The certifications that are needed to maintain a minimum number of certified people (eg to meet requirements for commercial partnerships and such) follow a slightly different process, but are treated in a similar way.

Finally, all other certifications are "**priority 2**".

Remember that certifications are a personal achievement and may have an impact on the company, so you are encouraged to insert them in your brag file (see [Engineering Ladder](EngineeringLadder.md)).

#### Priority 1 certifications process:
For this kind of certification there is no need for an approval process, but to benefit from the dedicated working hours to prepare the exam and the second attempt, it's required to notify the Certification Coach about the exam booking until there is still need for that specific certification.

1. Contact the Certification Coach to define a study plan and a feasible exam date. The chosen date should be within 6 months of entry, unless there are special needs.
2. Once done, update the certification plan cell with "Intention" status and the agreed approximate exam date.
4. Approximately a month before the expected exam date, ask to {% role %}Finance/Purchasing Officer{% endrole %} to pay (all costs will be covered) and book your exam. For guidelines of how to request purchases or reimbursements, please consult [Spending Company Money](SpendingCompanyMoney.md).
5. Once successfully booked, forward the Exam Registration Confirmation to the Certification Coach. If the number of certifications booked P1 before your exam date + the achieved of that kind is less than the number of needed certification, update the certification plan with the "Booked P1" status and the definitive exam date, otherwise set the "Booked P2" status and follow the remaining "Priority 2" certification process.
6. You can spend up to 24 working hours to prepare the exam, the remaining needed time should be your responsibility (Certifications are useful to the company, but in the end they are personal achievements). You can also use the Training Time Budget, see Training Budget section of [Benefits](Benefits.md). In case you need a second try there will not be available working hours.
7. Good Luck
8. Take the exam
9. Regardless if you fail or pass the exam:
    - communicate the result to the Certification Coach
    - update the status and date of your request in the certification plan
    - for the sake of knowledge sharing, always report the exam's content (and possibly related tips and tricks) on the company's Big Data OneNote, in a dedicated section (check if are there already sections for your specific exam, in that case append in there your experience).
10. If you passed the exam, drop an email to the Certification Coach, the CTO and Social Publisher attaching certificates or digital badges, providing or not the authorization to publish the achievement on socials.
11. In case you fail the exam, is not a problem, we can have another try.
12. In case you fail again, there will not be other paid chances unless we face specific needs.

#### Priority 2 certifications process:

1. Drop an email to the {% role %}Agile Lab/CTO{% endrole %} specifying the reason and the motivation to get that specific certification.
2. In case the certification you want is similar to a "priority 1" one, this will take precedence.
3. In case the requested certification is congruent with the technical landscape we manage in the company and the price is reasonable, it will be approved.
4. Meet the Certification Coach to define a study plan and a feasible exam date.
5. You don't have working hours dedicated to prepare such certification, but you can still use the Training Time Budget (see Training Budget section of [Benefits](Benefits.md)).
6. Ask to {% role %}Finance/Purchasing Officer{% endrole %} to pay ( all costs will be covered ) and book your exam. For guidelines about how to  make/request purchases or reimbursements, please consult [Spending Company Money](SpendingCompanyMoney.md).
7. Good Luck
8. Take the exam
9. Regardless if you fail or pass the exam:
- communicate the result to the Certification Coach
- update the status and date of your request in the certification plan
- for the sake of knowledge sharing, always report the exam's content (and possibly related tips and tricks) on the company's Big Data OneNote, in a dedicated section (check if are there already sections for your specific exam, in that case append in there your experience).
10. If you passed the exam, drop an email to the Certification Coach, the CTO and Social Publisher attaching certificates or digital badges, providing or not the authorization to publish the achievement on socials.
11. In case you fail, there will not be other paid chances.

# How to maintain the minimum certification levels

The certification coaches continuously track the expiration dates of all the certifications and the current certification numbers.

In case of certification expiration:
1. When the expiration date of a certification that must be maintained draws close (< 4 months), the certification owner will be contacted in order to organize the next steps
2. Depending on the type of certification and its relevance to the current career of the certification owner, the certification can be renewed as-is, another higher-level one might be chosen in its place, or it can be dropped if no longer relevant
3. Once the certification target has been defined, it is treated as a "priority 1" certification

In case a certified person leaves the company:
1. The certification coaches and the CTO decide if and how to compensate for the lost certifications
2. Either the "priority 1" certifications are extended to include the lost ones, or they are dropped if no longer relevant

# Where to find already available training material

In order to improve and speed up the search for training material already purchased or available, in the "Big Data" Sharepoint under the `Certifications` folder, you can eventually find a folder containing it. 

The convention is to specify the Fully Qualified Name of the exam (for example CCA 175 Spark and Hadoop Developer).

If you are aware of any previous material in another SharePoint folder, it is strongly recommended that you move it to the folder specified above.

# How to buy course material

If you need to buy course material related to certifications (books, video courses) you can buy it yourself with a personal account and then log it as "Expenses" on ElapseIt.

For guidelines about how to request/make purchases or reimbursements, please consult the Training Budget section of [Benefits](Benefits.md) and [Spending Company Money](SpendingCompanyMoney.md).

# Certification coaches

The current process followed by the certification coaches is hereby described.

#### Monitor the Bigdata/certifications teams channel

Monitor the `Bigdata/certifications` teams channel and provide feedback to everyone there

#### Begin of the year, a new goal is set by the CTO

When the `CTO` role sets a new certification goal for the year please create a new plan document at 
`sharepoint://BigData/Documents/Certifications/Certification Plan $YEAR`, you can clone the document of the past year.

Announce it by sending a mail to the `bigdata` mailing list (associated with the sharepoint group `BigData`) to reach every company member

#### Weekly check

Once a week remember to check the document located at `sharepoint://BigData/Documents/Certifications/Certification Plan 202x` and perform these actions:

* contact any candidates that didn't inform you about their study plan
* If there are unclaimed certifications (no one is actively working to take it) try to find candidates and repeat the announcements
* If there are approaching estimated exams dates please contact directly the candidate and ask if there are any blockers or things you can help with, if the candidate seems to be simply unmotivated or stressed propose switching to an "easier" certification or handing over the "reservation" to another more willing agile lab employee (do not blame the person but try to find out which part of the process is not working and why)
* If there are exams that have already been taken (if you heard about it or received partial information) but no success has been recorded or you did not receive the certificate remind the candidate of the missed parts of the process.

#### When a candidate contacts you

* Remind them of this document and to try to follow the process described in this document.
* Provide guidance on certifications you have taken yourself
* Facilitate the candidate by checking who already got the certification, make them talk to each other.

#### How to record achieved certifications

Other than the public certification plan `sharepoint://BigData/Documents/Certifications/Certification Plan 202x` please maintain the `Training` scoped document located at `sharepoint://Training/Documents/Certifications/202x_AGILELAB_Censimento Formazione` with expiration, license number and other exams details, store the certificates received via mail under `sharepoint://Training/Documents/Certifications/certificates`

Ask for previous certification obtained by new Agile members before being hired.

Always remember to give a big shoutout to certification achievers on the `thanks` team channel to motivate everyone.