# Introduction
Meetup events are considered technical events for experts in the area, and their main purpose is the sharing of technical studies and the recruitment of highly skilled people.

The company organization provides for the management of meetups in rotation between Agile Lab offices.

The MeetUp groups hosted/maintained/owned by Agile Lab are:

- [Big Data & AI Milano](https://www.meetup.com/bigdata-ai-milano/)
- [Big Data & AI Torino](https://www.meetup.com/big-data-ai-torino/)
- [Bologna Big Data](https://www.meetup.com/bologna-big-data-meetup/)
- [Big Data & AI Veneto](https://www.meetup.com/big-data-ai-veneto/)
- [Big Data & AI Roma](https://www.meetup.com/big-data-ai-roma/)
- [Big Data & AI Puglia](https://www.meetup.com/big-data-ai-puglia/)
- [Big Data & More Catania](https://www.meetup.com/big-data-more-catania/)
- [Data Mesh Italia](https://www.meetup.com/data-mesh-italia/)

Agile Lab offers three different kinds of meetups. The [Event Specialist](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5e990048c96978341f6dac99) can choose the kind of meetup most suitable for the goal he/she wants to reach. The kinds of meetups are the following:

- **Hold My Beer**: the goal is to create an informal environment to talk about a deep technical discussion where the main focus is live-coding. After a brief introduction, we need to hold the speakers' beers to let them write some code with both hands. It's very important to stimulate group discussions together with the attendees, that should be free to speak whenever they want

- **Tech Interview**: the goal is to carry on an interview, led by an Event Specialist that moderates the discussion. The technical level is lower, and here we aim to invite CTO, CDO, etc as speakers

- **Tech & More**: We don't want to limit your creativity to a fixed set of ways of leading meetups, so here fall all the other kinds of meetups that come to your mind.

Every kind of meetups has its own logo, you can find them in `sharepoint://Marketing/Documents/Materiale grafico/LOGHI/AgileLab`.

## Find speakers

Sometimes finding someone that wants to speak in one of Agile Lab events is not easy.

Here there are some ideas that an Event Specialist can look at in order to find some candidates:
* Agile Lab colleagues (and people they know)
* Agile Lab article writers that have already a subject ready to be transformed into a talk.
* personal friends, ex-colleagues, members of online communities, etc
* university professors or internships directors (even from other cities / universities)
* PhD's students (they are usually good candidates since they can talk about their thesis)
* observatory members that we sponsor as a company
* clients (this can be very tough because of restrictions in subjects and/or projects)
* people active on social media (this is usually difficult because they already have other active channels)
* vendors (this can be difficult since we can have restrictions about which ones we can engage)
* participants of past meetups that gave their willing to become speaker (there is a list of these candidates in `sharepoint://Marketing/Documents/eventi/meetup/speaker-e-candidati.xls`)

An Event Specialist can also look at the previous speakers and sponsors Agile Lab had in the past events: a speaker that talked about a topic can be available to talk about the progress he/she made or even other topics. The list of speakers is available in the marketing SharePoint under `sharepoint://Marketing/Documents/eventi/meetup/speaker-e-candidati.xls`.

A good idea is anyway to share the available speakers between cities (if an Event Specialist finds someone available in a city, he/she should communicate it to all the others Event Specialists because the speaker can participate in an event in another city).
This is even more important if the event that we are organizing is a remote one where we don't need the actual presence of the speaker.

Remember that in case is it still complex to find a speaker there is always the possibility to relax the constraint and organize talks that are less technical and that could be even interesting for a broader audience.

## Process

Below is described the process that considers only web meetups and that will be updated when you start to create events in presence:

IDEA and PLANNING: Every 6 months a meeting is scheduled between Event Specialists and [Planners](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5ebd62acc30c6135615df4c3) to organize the upcoming meetups. The two meetings will schedule:
- all events from September to January (meeting in July) 
- all events from February to August (meeting in December) 

The meetups will be scheduled following people's commitments and holiday calendars. Each Event Specialist will create a project on Holaspirit to allow the monitoring of the organization in the future.

## Realization

The tool chosen for meetups is Teams.
The realization of the meetup comes alive as the event approaches. 

**2 months before the event it is necessary that**:

- the Event Specialist creates the event in the draft on meetup.com
- the Event Specialist defines the topic of the meetup
- the Event Specialist chooses the external guest and contacts him/her to have the presence confirmed.
- the Event Specialist in collaboration with the Planner, choose the precise date of the event and fix it on the Outlook marketing calendar

**1 month before the event**:

- the Event Specialist publishes the event on meetup starting from the previous draft
- the [Social Publisher](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5e9904eea4825b680f05b539) advertises the event on its favorite social channels 
- Event Specialist and Social Publisher write the survey to be proposed to all the attendees at the end of the talk
- if needed, the Event Specialist can request to the [Servizi Aziendali role filler](https://app.holaspirit.com/o/5d934a98a626a353151327d5/roles?roleId=5eb426fddc350c000d0a6c35) to create an account for external speakers
- if needed, the Event Specialist can request to the [Servizi Aziendali role filler](https://app.holaspirit.com/o/5d934a98a626a353151327d5/roles?roleId=5eb426fddc350c000d0a6c35) to create the live event providing the following information:
  - Event title
  - Event description
  - Date, time, and duration of the event
  - If the event is public or internal
  - If the attendants will be able to register for the session
  - Enable/Disable Q&A
  - email address of all the presenters

Few days before the event, the Event Specialist and the Social Publisher should take care to promote the meetup on our social networks.
All the meetups should be recorded by the Event Specialist.


## Follow up

The days after the event, the Social Publisher will send thanks to the participants while the [Social Analyst](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5e9904eea4825b680f05b53d) will retrieve the metrics and update the CRM with the leads obtained.

The Event Specialist should upload the meetup recording to Microsoft Stream in the channel `Marketing`, setting a [Video Editor](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5e990048c96978341f6dacab) as video owner. 

The Event Specialist should collect the result of the survey and:
- fill the file in `sharepoint://Marketing/Documents/eventi/meetup/speaker-e-candidati.xls`
- communicate any hiring opportunity to the [HR circle](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/circles?circleId=5e98e9adaea15e43c57d6146)