
If you are reading this, probably is a sad moment because you or someone else is leaving.
Agile Lab does not provide any context around why people are leaving when they do, not for a lack of transparency but to preserve the individual privacy. 


Here you can find a good way to manage this important moment of your career:

1.  Don't communicate it to the team or your friends before to talk with the HR/COO/CEO
2.  Ask for a 1-1 meeting with the HR/COO/CEO mentioning the fact you want to quit, this will elevate your priority to the maximum level.
3.  Evaluate with attention the counteroffer
4.  In case you decide to leave, agree with your manager the next steps with the customer: your last day on the project and your last day in the company
5.  Decide how and when to communicate it to the team ( is your own responsibility ), but after the company eventually communicated it to the customer.

On your last day in the company:

* [ ]   Compile elapseit for what's left in terms of worked days
* [ ]   Fill in the Excel `sharepoint://BigData/Documents/Sito Internet/Sito_paginaTeam.xlxs` changing for your row the column `Status` with `ToBeDeleted`
* [ ]   Delete your entries in the Skill matrix
* [ ]   Send an email to the office365 admin saying that you are leaving (your account will be deleted by the end of the day)
* [ ]   Send an email to the Publisher admin saying that you are leaving (your name and photo will be deleted from the team page)
* [ ]   Send an email to the Training lead link saying that you are leaving (your LMS account will be deleted by the end of the day)
* [ ]   Send an email to the Elapseit admin saying that you are leaving (your account will be deleted as soon as not necessary any longer)
* [ ]   Send an email to the Internal IT device manager and to Welcome Pack & Replacement, saying that you are leaving (he/she will reach you out in order to properly dispose your devices).
* [ ]   Delete all company data on your external drives or other cloud accounts, if needed
* [ ]   Send an email saying goodbye to everybody (not needed but more than welcome and appreciated)
* [ ]   Detach your devices from Agile Lab account
* [ ]   Dive into your next adventure hoping this has been fun and rewarding
