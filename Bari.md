# Bari

- Address: [The Hub Bari srl Via Volga c/o Fiera del levante pad.129 70132 Bari](https://goo.gl/maps/EkmNWHEyficEZJSc9)
  - [Website](https://bari.impacthub.net/)
- Parking: Free
- By bus:
  * Amtab Line 22 stops at *Fiera Fronte*
  * Amtab Line 53 stops at *Fiera Fronte*
  * Local Bus [link](https://moovitapp.com/bari-3342/poi/Impact%20Hub/Piazza%20Aldo%20Moro/it?tll=41.13664_16.83734&fll=41.118698_16.870135&t=1)


- Seats: 3
- Rooms: 
    * 1 open space with 3 desks
    * 2 conference rooms (to be booked in advance - please refer to the Bari [Facility Manager](https://app.holaspirit.com/o/5d934a98a626a353151327d5/governance/roles?roleId=5e99064a094ee80565620bab) for instructions)
