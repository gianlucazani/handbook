# People Growth

Caring about people growth is one of the key elements of Agile Lab to be successful.

Making people advance in their career path is a valuable iteration of everyone's professional life and, it contributes to our growth as humans as well.

There are many ways to make people grow and here we will discuss how Agile Lab cares about _agilers_.

At Agile Lab we approach people growth in many ways:
- **Tutorship**: teaching people new things driving them through a personal path; usually short-term and laser focused on specific topics;
- **Mentorship**: mentors lead new _agilers_ by examples. Mentors are expected to proactively teach things, explain concepts, help resolving issues. 
  In turn, mentees are expected to be eager to learn, proactive asking questions, experimenting, failing to understand and improve.
- **Coaching**: a coach provides direct hints, recommendations, suggestions, feedback. It does not take direct actions on behalf of a coachee.  

## Tutorship

People growth starts since the first day at Agile Lab.
Everyone onboarding our company receives an initial tutoring by our [Buddies](Buddies.md).
At this stage we introduce new employees our culture, practices, tools and all the necessary means to be autonomous working on a daily basis.
This is a fundamental part of people growth since this gives everyone an initial taste of our identity and values.

## Mentorship
A mentor follows people growth for a mid-long overlap of their working life. 
Mentorship is more directive since mentors know which results to expect from their mentees.
Usually, mentorship is provided by peer programming, shadowing or any other activities done side-by-side with the mentee.
Mentorship tends to be more directive to address specific problems tries to tackle software design issues, troubleshooting, etc.).
Anyway, our mentorship style is strongly based on the proactivity of our mentees.
Thus, we expect our _agilers_ to work at their best and tackle their own issues with professional attitude.

## Coaching
A coach stimulates people brain, makes her/his colleagues growth giving hints, recommendations, providing feedbacks, and challenging their decisions.
Of course, our mantra is "leading by example". Anyway, coaching should not invade people's space in order to let them learn from their own experiences.
Instead, coaching means moving the right human and professional levers by sharing thoughts, reasoning together and making the right questions. 
For instance, a brilliant engineer moving to her/his first project leadership experience may need some initial shadowing (closer to mentorship than coaching).
As soon as she/he gets the right grip to move autonomously, coaching comes into play.

# Leadership
How does leadership matter with people growth?

At Agile Lab, we give [leadership](LeadershipAndCoaching.md) very much attention.

**Leadership is the ability to influence opinions, decisions, people.**

There are good leaders and bad leaders. We aim to grow wonderful leaders and this requires an important dose of people growth effort.

We excercise our leadership to provide our customers with elite data engineering principles, practices, services, and products.
We want our professionals being able to manage complex initiatives, projects and situations while feeling safe and confident about their actions taken with awareness about risks and benefits.
Our leaders work to growth new leaders and build a community of professionals.
Building leaders means nurturing self-actualization based on a solid methodological, technical, and organizational background.
Our leaders convey tutorship, mentorship and coaching at many levels.

Whether behaving as mentors or coaches is inherently situational.
It depends on seniority of trainers and trainees, type of activities and accountabilities to make people feel safe with.  

**For this reason, you may find in the handbook the terms mentor, coach and leader used with the same intention: leaders caring about people growth.**

![alt text](images/coaching-vs-mentorship.png)

## Lead Links
A lead link is in charge of the management of a piece of the organization following our self-management principles as stated in Agile Lab's [constitution](./Constitution.md).
Lead links should mentor and coach their circle members with particular attention to [team leaders](#team-leaders) since they are responsible for a whole team and may need more support.

## Team Leaders
Our activities are often organized by teams. Every team is assigned to a [Team Leader](LeadershipAndCoaching.md) responsible for team members' growth.
A team is always an important occasion to learn for everyone. Team leaders can learn how to mentor/coach our engineers and engineers can learn by examples from our team leaders.
Usually this is a mid-long term path where _agilers_ have a constant occasion to improve and receive feedback.
A team leader helps her/his colleagues to ramp up their [career path](EngineeringLadder.md) through the achievement of goals within and outside the scope of their own projects according to company's objectives and [purpose](Company.md).
This is very important to create alignment between personal/professional expectations and company's objectives.

## Mentorship in team working
The team leader is not the only mentor within a team.
In a team, people having more experience care about younger _agilers_ to make a team stronger and more effective.
An effective team makes everyone improve. People eager to improve makes a team stronger.

## Community Of Practices
At Agile Lab we have established a [Community Of Practices](./CommunityOfPractice.md) for [Leadership & Coaching](./LeadershipAndCoaching.md) where to discuss how to improve our practices for people growth.