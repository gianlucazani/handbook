# Website Procedure 

## Periodic Checks
As needed and at least every quarter, the Compliance Team will ensure the verification of data flows and processing procedures, as well as all the documentation (notices and consent forms) present on the website(s) used by the Company at any given time.
This verification will be conducted using the attached checklist.
The Compliance Team will be responsible for reporting any necessary updates or changes and ensuring their implementation.
The Compliance Team will be required to make any necessary updates to Agile Lab's Records of Processing Activities.

The Website Check List is available on the Compliance Sharepoint.