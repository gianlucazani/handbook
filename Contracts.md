
In Agile Lab we employee people from all the world and we can configure them in several ways.

We have Employees and Contractors. But then these can be divided in different sub-categories.


E1: Employee with permanent contract in Italy
E2: Employee living abroad that is invoicing Agile Lab directly
E3: Employee hired by an intermediary like Horizons, Deel, that are only taking care of payroll and legal entity

C1: Contractor long-term. They are hired from some other organization that is running their operations. This is staff augmentation, but anyway we always try to build long-term relationships
C2: Contractor short-mid term. Freelances or other people belonging to other organizations and working on a specific project


Here is a resume of the differences between all these categories.

|                         | E1     | E2     | E3     | C1     | C2     |
| ----------------------- | ------ | ------ | ------ | ------ | ------ |
| Ladder                  | YES    | YES    | YES    | NO     | NO     |
| Coaching                | YES    | YES    | YES    | YES    | NO     |
| Training Budget         | YES    | YES    | YES    | YES    | NO     |
| Team Building           | YES    | YES    | YES    | YES    | NO     |
| Company Meetings        | YES    | YES    | YES    | YES    | NO     |
| Ticket Restaurants      | YES    | NO     | NO     | NO     | NO     |
| 8% bonus                | YES    | YES    | YES    | NO     | NO     |
| Heanlth Insurance       | YES    | NO     | NO     | NO     | NO     |
| Welcome Pack            | YES    | YES    | YES    | YES    | NO     |
| Intune                  | YES    | YES    | YES    | YES    | NO     |
| BigData Group           | YES    | YES    | YES    | YES    | NO     |
| LMS                     | YES    | YES    | YES    | YES    | NO     |
| SmartWorking+           | YES    | YES    | YES    | NO     | NO     |
