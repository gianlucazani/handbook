# Leadership & Coaching

# Leadership

**Leadership is the ability to influence opinions, decisions, people.**

There are good leaders and bad leaders. 

We aim to grow wonderful leaders:

- Acting with awareness, respect and method.
- Being ambassador of [Agile Lab's values](./Values.md).
- Feeling and acting as [mentors and coaches](./PeopleGrowth.md) and striving for continuous improvement.
- Behaving consistently inside and outside the company. We like people that do not fake. If you don't feel your values matching our values this is fair, feel free to leave.
- Being supportive with respect to each other. We do not leave our colleagues alone. We always share risks, joys and failures.
- Feeling free to disagree, bringing in your opinion, being constructive.
- Seeking truth over cohesion.
- Striving to make the organization simpler.
- Acting as servant and gentle leaders. Don't feel a boss, none of us does.

## Good leadership 
- Managing under-performance is one of your most important tasks as a manager.
- When times are great, be a voice of moderation. When times are bad, be a voice of hope.
- If you praise someone, try to do it publicly and in front of an audience. If you give suggestions to improve, do it privately 1 on 1.
- Understand that there are different ways to get to the same goal. There are different perspectives, and discussions need to happen.
- When someone says they are considering quitting, drop everything and listen to them. Ask questions to find out what their concerns are. If you delay, the person will not feel valued and the decision will be irreversible.
- If you are asked why someone has left or is leaving, please refer that person to the general guidelines section of the handbook where we describe what can and cannot be shared.
- People should not be given a raise or a title because they ask for it or threaten to quit. We should proactively give raises and promote people without people asking. If you do it when people ask, you are being unfair to people that don't ask and you'll end up with many more people asking.
- Don't refer to Agile Lab as a family. It is great that our team feels like a close-knit group, and we should encourage that, as this builds a stronger team. But families and teams are different. Families come together for the relationship and do what is critical to retain it. Teams are assembled for the task and do what is required to complete it. Don't put the relationship above the task. Besides, families don't have an offboarding process. Families should have unconditional love, while teams have conditional love. The best companies are supporters of families.
- Praise and credit the work of your reports to the rest of the company, never present it as your own. 
- Try to be aware of your cognitive biases.
- Combine consistency and agility.
- Do everything to unblock people. If someone has a question that is keeping them from being productive, try to answer the question yourself or find someone who can.
- "People either get shit done or they don't. And it's easy to be tricked because they can be smart but never actually do anything." Watch for results instead of articulate answers to questions, otherwise you'll take too much time identifying under-performers.
- We don't have explicit 20% time at Agile Lab. We measure results and not hours.
- "Always tell us the bad news promptly. It is only the good news that can wait." Make sure to inform your manager of bad news as quickly as possible. Promptly reporting bad news is essential to preserving the trust that is needed to recover from it.
- Complain up and explain down. Raise concerns you hear to your manager. When peers or reports complain, explain why a decision was made. If you don't understand why, ask your manager.
- Train your team and build their confidence until you are in the position to trust them.

## Good/bad team leaders

Good team leaders:
- full responsibility and measure themselves in terms of delivery and customer satisfaction.
- define their job and their success. 
- are resilient, they are able to adapt to mutable scenarios and customer requests. 
- establish good relationship with the customer from day 1.
- clearly define "what" instead of "how".
- provide to the team a clear definition of done.
- don't give direction informally. They gather information informally.
- communicate a lot with the customer, understanding and anticipating needs, raising and solving problems with transparency.
- send their status report frequently because they are organized and disciplined. 
- ask for help as soon as they need.
- act keeping in mind company's values and processes. 
- provide blameless support to the team. 
- collect and provide feedbacks doing 1-1 meetings and retrospective. Negative feedbacks always in 1-1.
- assume positive intent on team mistakes

Bad team leaders:
- constantly want to be told what to do
- always complains about the customer and don't modify their modus operandi
- have lots of excuses
- complain about the team
- don't follow company guidelines and processes

# Coaches
A good coach is expected to:
- Understand and be familiar with her/his domain and context
- Be positive, make always possible a path for improvement
- Be assertive and goal-oriented
- Excel in clarity and patient
- Ask the right questions, stimulate reasoning
- Listen actively and sincerely
- Spend time with coachees, be available, do not make coachees wait forever for guidance
- Be supportive, keep judgements aside
- Provide examples close to the coachees' experiences, otherwise she/he will not be understood
- Do not act on behalf of the coachee, trust coachees
- Do not scratch the surface, be a profound thinker, go to the root of the issues
- Be an observer, explain issues consistently and in depth, do not be superficial and generic with coachees
- Share knowledge and take time to understand how to transfer knowledge
- Keep improving on hard and soft skills, keep her/himself up to date, be a beautiful example
- Motivate and energise your coachees
- Motivate through emotions, teach through facts
- Strive for confrontation with other coaches
- Display genuine passion and commitment on nurturing colleagues and caring about people growth

A bad coach:
- Does not listen carefully to coachees
- Provides rough suggestions
- Uses to blame and finger-point
- Spends time talking about her/his stories of success for no reasons
- Does not trust coachees
- Does not motivate people or motivation is driven by fake values
- Does not share knowledge with coachees and coaches

# Other references
- [Buddies](./Buddies.md)