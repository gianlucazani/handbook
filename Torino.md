# Torino

- Address: [Corso Trapani 16/1, Fifth floor, 10139 - Turin ](https://goo.gl/maps/ETJNB7bfep7KNSdH8)
- Parking: free spots reserved in the underground garage. Just enter with your car and search for the Agile Lab logo on the wall indicating our reserved spots. 
- Bike parking available
- Public transportation in Turin is handled by [GTT](https://www.gtt.to.it/cms/):
  - By metro: 
    * *Rivoli* stop, then three minutes by walking. Best option if you're coming from the train stations (both Porta Nuova and Porta Susa) and from the airport (airports shuttles stop at the train stations).

  - By bus:
    * GTT Lines: 68, 55, 2, 22

  It is possible to purchase GTT tickets for the metro or bus downloading the [*GTT TO move*](https://play.google.com/store/apps/details?id=it.to.gtt.tomove&hl=it&gl=US) app (only for Android users).

- Office spaces: 
    * _Operative area_: 15 desks in an open-space, fully equipped with external screens, keyboards, mouses, headsets and fast wi-fi connection. Free coffee, tea and water.
    * 1 conference room, also usable for private calls and meetings.
    * _Recreative area_: coffee zone, sofas, a big TV, speakers, Xbox, ping-pong table, guitar. Definitely our favorite area :)
