# Introduction

Generating the newsletter helps keep communication alive with anyone who has come into contact with Agile Lab and agreed to receive communications.


# Process

The newsletter is scheduled every 2 months: January, March, May, July, September, November and is sent on the last days of the month.
The mailing is unique, without distinction of role, interest or company of the recipients and for this reason it has been chosen to collect different topics.

In the weeks preceding the mailing, the content owners, in collaboration with those who work on lead generation and SEO and who can therefore give indications on the topics of the greatest interest, define the contents of the newsletter by specifying:
* title
* short description (only one sentence)
* image (if applicable)
* link (if applicable)
* topic

The topics currently available are:
* HIGHLIGHT: the most significant news from the Agile Lab world and beyond
* EVENTS: past events in the 2 months before posting and future scheduled events
* TALK ABOUT US: mentions received by Agile Lab in newspapers or websites
* THE TEAM: new recruitments
* WORLD NEWS: world news that we consider important, related to Agile Lab's work and that we want to bring to the attention of our readers
* THE BOOK: books of interest that we are pleased to point out, rather than the book that is being read by the corporate group at the time
* MAYBE YOU MISSED: news already communicated in the previous newsletter and that we consider important.

The newsletter is also published to the site on the page agilelab.it/newsletter, a page from which it is also possible to subscribe.

The person with the role of Publisher is in charge of sending the newsletter to the subscribers.
