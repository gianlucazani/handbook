In Agile Lab we don't want to incentivize to work overtime because in the long period can lead to burnout, and we care about our people. 
Our working hours are flexible, and people are strongly encouraged to balance work and personal life priorities.
Each individual must understand when to put more effort into the job and when it is possible to regain space in private life. We want people to be free to organize their activities at their best without any constriction but always acting with self-responsibility.

If you want to focus more on the job to hit a goal, let's say an intensive week, we strongly recommended to slow down and recover during the following days. You don't need to notify anybody. If you have some credit, you can spend them whenever you want.

On average, the balance ***must be*** neutral.

Anyway, you can face challenging situations where it is difficult to keep such balance. In that case, you have to notify your manager immediately and start to put those hours in the timesheet labeled as "SA-Straordinari ad accumulo".

Your manager will review them periodically and will try to find a way to reduce the load and give back those hours. At the end of the semester, anyway exceeding hours will be paid.

This mechanism is to incentivizes people not to work overtime.
