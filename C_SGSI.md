Agile Lab S.r.l. has the strategic mission of providing Big Data and Analytics, Machine Learning, Edge AI, IoT, Low Latency Data Streaming, and Domain Driven Design services, as described on https://www.agilelab.it.

In line with its strategic mission, the Company has planned its organizational structure, which includes the adoption of specific management systems. Considering the aforementioned strategic mission, the adoption of an Information Security Management System in accordance with the ISO 27001 standard is of particular relevance. By adopting a modern information security management system, the Company aims to achieve the following objectives:

Ensure compliance with security requirements applicable to the services provided to its customers, including the quality management system adopted.
Ensure compliance with the requirements regarding the protection of personal data as stipulated in the European Regulation 679/2016 ("privacy").
In this perspective, the Company has described in a specific document the configuration of the security perimeter for information management and the information that this perimeter protects.

PURPOSE AND SCOPE
1.1 PURPOSE

This document describes the security perimeter that contains the information that the Company considers particularly relevant in line with its strategic mission.

The information security perimeter is structured into three different dimensions and the relationships between them:

Organizational dimension
Physical dimension
Logical dimension
The information security perimeter defines the purpose of the information security management system adopted by the Company in accordance with the ISO 27001 standard.

1.2 SCOPE

This document applies and is referenced by the following management systems adopted by the Company:

Information Security Management System ISO 27001
Personal Data Security Management System in accordance with the European Regulation 679/2016
Risk Management System in accordance with the ISO 31000 standard
Quality Management System ISO 9001
Business Continuity Management System ISO 22301
REFERENCED STANDARDS
This document refers to the following standards:

UNI EN ISO IEC 27001:2017 "Information technology - Security techniques - Information security management systems - Requirements"
ISO IEC 27004:2016 "Information technology - Security techniques - Information security management - Monitoring, measurement, analysis, and evaluation"
UNI ISO 31000:2010 "Risk management - Principles and guidelines"
ISO IEC 27005:2011 "Information technology - Security techniques - Information security risk management"
UNI CEI ISO/IEC 20000-1:2012 "Information technology - Service management - Part 1: Service management system requirements"
ISO 22301:2012 "Societal security - Business continuity management systems - Requirements"
European Regulation 679/2016 Privacy
TERMS AND DEFINITIONS
The document provides the definitions of the acronyms and terms used, including:

SGSI: Information Security Management System (according to ISO 27001)
MSGSI: Information Security Management System Manual
IT: Information Technology
Process: Set of related or interacting activities that transform input elements into output elements
Information: Meaningful data
Information Security Management System: Part of the overall management system, based on a risk approach, to establish, implement, operate, monitor, review, maintain, and improve information security
Effectiveness: Degree of achievement of planned activities and planned results
Efficiency: Ratio of achieved results to resources used
Information Security: Preservation of confidentiality, integrity, and availability of information, as well as other properties such as authenticity, accountability, non-repudiation, and reliability
Availability: Property of being accessible and usable upon the request of an authorized entity
Confidentiality: Property that information is not made available or disclosed to unauthorized individuals, entities, or processes
Integrity: Property of safeguarding the accuracy and completeness of assets
Authenticity: Property that an entity is what it claims to be
Accountability: Responsibility of an entity for its actions and decisions
Non-repudiation: Ability to prove the occurrence of an event or action and its originating entity to resolve disputes regarding the occurrence or non-occurrence of the event or action and the involvement of the entities in the event
Reliability: Property of consistent behavior according to expected results
Document: Information and its supporting medium
Policy: Formally expressed management directions
Procedure: Specified way to perform a process or activity
Record: Document that reports obtained results or provides evidence of performed activities
IDENTIFICATION AND CLASSIFICATION OF INFORMATION TO BE PROTECTED
Information related to:

Personal data (personal information, bank details)
Customer data (personal data)
Health-related personal data (occupational safety medical records)
Telephone traffic (call detail records for billing purposes)
Telematic traffic (emails, connection sessions)
ORGANIZATIONAL PERIMETER
The organizational perimeter of information security identifies the entities whose behavior has a direct impact on the security of corporate information. These entities can belong to the Company (internal organizational perimeter) or be external to the Company (external organizational perimeter).

CORPORATE PERIMETER

The information security perimeter includes company functions belonging to Agile Lab S.r.l. as listed in the table below:

COMPANY	LOCATION	CONTROL
Agile Lab S.r.l.	Via Montefeltro 6, 20156 – Milano - IT	Development – Management
Agile Lab S.r.l.	C.so Trapani 16/a – 10139 Torno - IT	CTO – Design and Development – IT
Agile Lab S.r.l.	Via Ugo Bassi 1 – 40121 – Bologna - IT	Design and Development
In this document, the term "Company" should be understood as described above.

INTERNAL ORGANIZATIONAL PERIMETER

5.2.1 ORGANIZATIONAL STRUCTURE

The following organizational chart describes the Company's organizational structure. The organizational units within the scope of the ISO 27001 ISMS are highlighted.

Organizational structure:

[Organizational chart (refer to SharePoint circles)]

5.2.2 COMPANY FUNCTIONS INCLUDED IN ISMS

The internal organizational perimeter of information security