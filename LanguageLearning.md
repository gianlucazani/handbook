#Language Learning
Agile Lab has adopted English as the official language.
Some people may not feel too confident speaking English, or someone may want to work on her/his accent (like the Italian one). For this reason we introduced the possibility to improve our language skills using a platform that offers 1-to-1 sessions with native speakers.

##The platform
Each employee can decide to dedicate a part of his/her budget to the platform.
It is suggested to plan at least 1 hour per week for 3 months.

The platform is also available as B2C, but we've activated the enterprise (B2B) version that brings us some extra features like:
- an initial assessment test
- one test every 8 hours lessons to measure improvements
- extra study material
- managing the budgets of each (you do not have to advance money)
- a dashboard for monitoring features
- more flexibility in the way we can spend money
- have unified billing for Agile Lab, but anyone can add extra funds in case of more time to be spent
- we have a personal "success manager" to talk to in case of any issues

The enterprise access has a fee which is a +18% on the cost of the lessons. As of now, this extra fee is to be intended as part of the personal budget that will be spent.
Our platform contact suggest that, in order to really have consistent improvement, a language should be practiced with continuity and we totally agree on that. For this reason, they suggest this approach:
1. people who want to improve, must commit for a period of 3 months.
2. they receive a credit on the platform (paid by AgileLab) of a certain amount (e.g. for 3 months, 1 hour a week for a total of 12 weeks at average cost of 15€/hour = 180€ + 18% = 212,40€).
3. that amount should be spent within the period of point 1. As enterprise account, we can ask explicitely to have 1 extra month of validity for the budget if someone have issues in consuming it within the standard period (but it should be considered a corner case).
4. everyone can spend that budget the way they consider appropriate (e.g. 2 lessons of 30 minutes per week? 1 lesson of 60 minutes per week? pick a tutor that costs 30€/hour? it's your choice..). If you're not satisfied with the tutor you have chosen, you can change it anytime and the first lesson can be reimbursed.
5. Everyone can add more funds with their own credit card if they want to invest personal money to improve even more

Here are some details on how it works:
https://agilelab.sharepoint.com/:w:/s/training/EWfuTtD8yAhOguP7WqK8wC0BPppwxQ4I3VNrC58xFstkvA?e=n8lEgx

There is also feedback from who has already tried a lesson.

##Languages
At the moment it is supported only for English and Italian Learning.
English, as said before, is the official language of Agile Lab.
For not Italian people can be a good opportunity to learn the Italian language on the platform and after exercise it with other Italian employees during the job.

##Budget
Lessons cost money and time.
Fortunately, it is possible to use the (money and time) budget that Agile Lab allocates for training as explained in [benefits](Benefits.md).
At the moment there is the possibility to choose between two packages: 
- 3 months (180$ allocated in the platform, with a real cost of about 200€)
- 6 months (360$ allocated in the platform, with a real cost of about 400€)

This means that you have 3 or 6 months to consume your credit on the platform (considering an average cost of 15€/hour, it should be one hour a week). If you run out of credit first, you can add more.

##How you can add budget in the platform?
you should insert an expense on Elapseit with this information:
- name of expense: 'Language Learning budget 3 month (YYYYMM)'
- Amount: 203.67
- Type: 20) PURCHASED BY BUYER (RDA - RICHIESTA DI ACQUISTO)
- Project: '202X Agile Training - Personal Budget'
- Email for notification: training@agilelab.it

Once submitted the request, get the Elapseit's _"Expense ID"_ (e.g. `ELI_0001234`).
Then send also an email to {% role %}Training/Training Buyer{% endrole %} with the _"Expense ID"_ .

The budget will be allocated in a profile that will be automatically created using your Agile Lab email after some days.

Due to the fee of 18% and conversion dollars euro, if we allocate 180$ in the platform, you pay almost 204 euro.
You can use a part of the past Company Prizes to pay some lessons. In this case you should anticipate the money and ask a refund with a request of this type:
- name of expense: 'Language Learning budget n month (YYYYMM)'
- Amount: XXX
- Type:  07) RIMBORSO PIE' DI LISTA
- Project: '202X Agile General Internal Services'
- Email for notification: amministrazione@agilelab.it

##Review on our teachers
Internally, we're collecting reviews on our teachers using this excel https://agilelab.sharepoint.com/:x:/s/bigdata/EXFi2TMlcCZCrJvH0w86PCcBqV2WzZsFW4YgCPoDNFBGjg?e=hyQ6sJ

##Notes for Training Buyer
- When you receive a request, you should write an email to our contact with the platform and ask to allocate the budget. He/she'll do it and also will issue an invoice.
  Unfortunately, you can't pay it (because it's necessary to do it by wire transfer) and so you should contact {% role %}People Ops and Care/Buyer{% endrole %}.
- If possible, don't send too many requests in the same period. For invoice management (of the platform but probably also for us), it's better to accumulate the requests and send them once per two weeks.